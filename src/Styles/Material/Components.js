export default {

	button:(type)=>{
	return`
	  position: relative;

	  padding:  0.8rem 1.4rem;
	  border: none;
	  display: inline-block;

	  border-radius: 5px;

	  cursor: pointer;

	  font-weight: 500;
	  color: white;

	`},
	floating:(type)=>{
		return `
			border-radius: 100%;

			width: 56px;
		    height: 56px;
		      
		    cursor: pointer;

		    display: flex;
			text-align:center;
			justify-content: center;
			flex-direction: column;
			align-items: center;

			color: white;
		`
	},

	icon:()=>{
		return ``
	}

}