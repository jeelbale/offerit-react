import React, {useState, useContext} from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import OutlineEdit from "react-md-icon/dist/OutlineEdit";
import OutlineSearchIcon from "react-md-icon/dist/OutlineSearch";
import RoundMenuIcon from "react-md-icon/dist/RoundMenu";
import { Link } from 'react-router-dom';
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import MorePopover from "./MorePopover"
 import {SteemContext} from 'react-steem-provider';

const Input = styled.input`
width: 300px;
margin: 0 auto;
  border: none;
  background: #f1f3f4;
  //border-bottom: 1px solid rgba(0,0,0,0.3);
  color: rgba(0,0,0,0.5);
  padding: 0.6rem 0.8rem;
  font-size: 14px;
   box-shadow: 0 0.3px 3px rgba(0,0,0,0.2);
   border-radius:17px;
  &:focus {
    outline: none !important;
    border:1px solid ${props=>props.theme.color.orange};
    
    }

`;

const LogoTitle = styled.div`
  color: ${(props)=>props.theme.color.orange};
  font-size: 22px;  
`;

 const Logo = styled.img`
  padding-top:3px;
  width: 30px;
  margin-left: -10px;
  //background: red;
`;

 const Nav = styled.nav`
  top:0;
  left:0;
  position: fixed;
  overflow: hidden;
  width: 100%;
  background: white;
  // border-bottom: 1px solid rgba(0,0,0,0.09);
  box-shadow: 0 0.5px 6px rgba(0,0,0,0.2);
  height: 60px;
z-index:999999999999999;
  color: ${props=>props.theme.color.navbarText};
  ${props=>props.theme.utils.rowContent()}

`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent()}
    width: 85.1%;
    margin: 0 auto;
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;

    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;

`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Item = styled.div`
    position: relative; 
    height: 60px;
    cursor: pointer;   
    color: ${props=> props.active?"black":"#5F6469" };
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 0.5rem;"}
    ${props=>props.hideBig && `
      display: none;
      @media (max-width: 700px)
      {
          ${props.theme.utils.centerContent()}
      }
    `}
    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `}

   
    a{
        text-decoration: none;
         color: ${props=> props.active?"black":"#5F6469" };
 
       font-weight: 500;
    }
`;
 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;
const Icon = styled.i`
  vertical-align: bottom;
`;
const TabSet = styled.div`
display: ${props=>props.notNavbar?"none": "contents"};
@media (max-width: 700px) {
  display: none;
}
`;


const TabItem = ({children, active, onClick})=>{
      
  const TabSelector = styled.div`
    background: ${(props)=>props.theme.color.orange};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 16px;

    
    border-radius: 5px 5px 0 0;
    bottom: 0;
    margin:0 auto;
`;

  return (
    <Item 
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer
      >
       {children}
        {active &&  <TabSelector/>}
    </Item>
    )
}


const logo = require("static/logo.png")


const Toolbar = (props)=> {

  const {
    loginUrl,
    auth,
    logout
   
    } = useContext(SteemContext);
    
 
  const [filter, changeFilter]  = useState("")
    return (  
      <React.Fragment>  
      <Nav>
        <NavContent>
        <AlignStart>
     
        <Item 
        style={{ 
          fontSize:"22px", 
          fontWeigth:"600",
          marginRight:"1rem",
        }}>
          <Link to="/">
            <LogoTitle>
              <Logo
                src={logo}/>
            </LogoTitle>

          </Link>
        </Item>

         <Item  

          style={{
            fontSize:"25px",
            fontWeight: "600"
          }}>
           <Link to="/"   style={{
            textDecoration:"none",
            color: "gray",
            fontWeight: "600"
          }}>
        OFFERIT 
        </Link>
        </Item>

     




        </AlignStart>

        <AlignEnd> 

{auth?
  <React.Fragment>
        { /*<Item margin>
                  <OutlineSearchIcon
                 style={{ 
                      fontSize:"30px",
                     
                    }}/>
                </Item>
        
         */}
        
                <Item>
       

        <Link to="/add-new">
          <OutlineEdit
         style={{ 
              fontSize:"30px",
              margin: "0 1.2rem"
            }}/>
            </Link>
        </Item>




       
        <Item>
        <MorePopover>
        {/* <MorePopover></MorePopover>*/}
          <RoundMenuIcon  
            onClicsk={logout}
            style={{ 
              fontSize:"30px"
            }} />
             </MorePopover>
        </Item></React.Fragment>:
      <React.Fragment>

        <Item margin>

        <a  href={loginUrl}>
     <b style={{opacity:"0.7"}}> Log in</b>
          </a>
        </Item>
        <Item margin>
        <a target="_blank" href="https://signup.steemit.com/?ref=offerit">
       <b style={{opacity:"0.9"}}>Register</b>
          </a>
        </Item>
        </React.Fragment>


      }


        </AlignEnd>
        </NavContent>
      </Nav>

      </React.Fragment>
  
    );
 }

export default Toolbar;

/*
<AddItem />
  <Item>
  <MorePopover>
  <UserCirlce />
  </MorePopover>
  </Item>
*/