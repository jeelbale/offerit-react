import React, { useState, useEffect, useContext } from 'react';
import styled, {keyframes} from 'styled-components';

import BaselineKeyboardArrowUp from "react-md-icon/dist/BaselineKeyboardArrowUp";
import BaselineKeyboardArrowDown from "react-md-icon/dist/BaselineKeyboardArrowDown";
import BaselineComment from "react-md-icon/dist/BaselineComment";

import {Link} from "react-router-dom"
import {timeAgo} from 'JS/date';
import {parserSteemRep, parserSteemSimpleRep} from 'react-steem-provider/Helpers';

import SteemAsync from 'react-steem-provider/SteemAsync';
import {SteemContext} from 'react-steem-provider';

const _ = require('lodash');
 
//Code
const CARD_HEIGHT= "430px"


const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 60%;
    background: #fefefe;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
    left: 20%;
    margin: 0rem auto;
  
    min-height: 800px;
    padding: 2rem;
 
   
    padding-bottom: 50px;
    text-align: left;
  
    @media (max-width: 700px)
    {
         width: 100%;
         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;
 const Title = styled.h1`
  margin: 0;
  font-weight: 500;
  font-size: 18px;
  width: 90%;
  margin: 1rem auto;
  color:black;
  text-align: left;
  line-height: 1;
`;

const Subtitle = styled.h2`
  margin: 1rem auto;
  font-weight: 400;
  font-size: 14px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;






const Card = styled.div`
  width: 100%;
  margin: 1.2rem auto;
  
  background: white;
  border-radius: 2px;
  border: 1px solid rgba(0,0,0,0.1);
  text-align: center;
  overflow: hidden;
  position: relative;
  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }
`;



 const Row = styled.div`
  width: 90%;
  margin: 0 auto;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;




const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 22px;
  opacity: 0.9;
  text-align: left;
`;
const ImgContainer = styled.div`
width: 100%;
height: 250px;
overflow: hidden;
background: #3bc2b8;

 @media (max-width: 700px)
    {
         height: auto;

         
    }

`;


const Img = styled.img`
  width: 100%;
 
`;


const Item = styled.div`

  margin-right: 0.5rem;
  height: 60px;
  ${props=>props.theme.utils.centerContent()}
    @media (max-width: 700px)
    {
        width: 100%;
       
         
    }
 
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;

    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;

`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;


const Divider = styled.div`  
  width:1px;
  background: rgba(0,0,0,0.1);
  height: 30%;

`;
const Circle = styled.div`  
  border:1px solid rgba(0,0,2,0.3);
  cursor: pointer;
  &:hover{
    background: rgba(0,0,0,0.4);
  }
  width: 20px;
  height:20px;
  border-radius: 50%;
   ${props=>props.theme.utils.centerContent()}

  ${props=>props.voted && ` 
     background: #b79ee6;
   color: white;`}


${props=>props.load && `background: green;`}

`;



const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;


const ImgCircleContainer = styled.div`  
  border:0;
  width: 36px;
  height:36px;
  border-radius: 50%;
overflow: hidden;
background: gray;
  border: 1px sold rgba(0,0,0,0.2);

  
`;

const Button = styled.button`

  padding: 0.6rem 1rem;
  border-radius: 7px;
width: 150px;

 
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.disbled ? "#879be4" : "gray")};
  color: ${props => (!props.transparent ? "white" : "#879be4")};
  border:${props => (!props.transparent ? "none" : "1px solid "+ "#879be4")};

   @media (max-width: 700px)
    {
        width: 50%;
        font-size: 11px;

         
    }
`;




 const Header = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 17px;
  width: 90%;
  margin: 1rem auto;
  color: rgba(0,0,0,0.7)  ;
  text-align: left;
  line-height: 1;

`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
    @media (max-width: 700px)
    {
     width: 100%;
         
    }
`;

const SubHeader = styled.h2`
  width: 90%;
  height: 3.4rem;
  overflow: hidden;
  margin: 1rem auto;
  font-weight: 400;
  font-size: 16px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;

/*
client.vote(voter, author, permlink, weight, function (err, res) {
  console.log(err, res)
});

*/

const ItemPostList = ({ followers,person, name})=>{

  const [loading, setloading] = useState(true);
  const [author, setAuthor] = useState("");
  const [follow, setFollow] = useState(false);
  const [disabeFollow, disabeFollowFollow] = useState(true);


    const {
      auth,
      actions,
      steemConnect
    } = useContext(SteemContext);

    const person_name =  followers?person.follower:person.following;


   useEffect(()=>{
    SteemAsync.getUserAccount(person_name).then(
    (response)=>{
      let profile_image = "";

      try{

         profile_image = JSON.parse(response[0]["json_metadata"]).profile.profile_image; 
       
      }
      catch(e){
        
      }



        setAuthor({name:response[0].name,reputation: response[0].reputation, profile_image}); 

    if(auth )
    SteemAsync.getFollowing(auth.name,1, person_name).then(
    (response)=>{ 
      
    if(response.length > 0 )
      if( response[0].follower === auth.name)
          setFollow(true)
      

    /*
      SteemAsync.getFollowCount(username).then(
    (response)=>{ console.log(response) }).catch(
    (err)=>console.error(err));
    */

    disabeFollowFollow(false)
    }).catch(
    (err)=>{
      console.error(err);
       disabeFollowFollow(false);

    });




       //setloading(false)
     }).catch(
      (err)=>console.error(err)) 

    },[])









  return (
      <React.Fragment>

            <Card to={`/profile/${author.name}`}>
            <Row>
              <AlignStart>

              <Item style={{width:"38px"}}>
             {author && <ImgCircleContainer><Img src={author.profile_image} /></ImgCircleContainer>}
              </Item>
              <Item> 
              <Link to={"/profile/"+author.name} style={{textDecoration:"none"}}>   
                <Title>
                {author.name}
                </Title>
              </Link>
              </Item>
              <Item huge> 
              <Link to={"/profile/"+author.name} style={{textDecoration:"none"}}>   
                ({parserSteemRep(author.reputation)})
              </Link>
              </Item>


              </AlignStart>
               <AlignEnd>
               <Item>
{auth && <Button

  disbled={disabeFollow}
  onClick={()=>{
  disabeFollowFollow(true)
  if(!follow)
  actions.follow(person_name).then((r)=>{
    
    setFollow(true)
    disabeFollowFollow(false)

  }).catch((e)=>{alert("Error following");
   disabeFollowFollow(false)
})
  else
  actions.unfollow(auth.name,person_name).then(()=>{

    setFollow(false);
     disabeFollowFollow(false);
  }).catch(()=>{ 
    alert("Error unfollowing");
     disabeFollowFollow(false);
  })
}}



  >{!follow?"Follow": "Unfollow"}</Button>}
               </Item>


              </AlignEnd>

              </Row>
            

  
            </Card>

     
      </React.Fragment>
    );
}

export default ItemPostList;


/*

(!this.props.orders.value.products[product.id] && !this.props.disbled) &&
  <Button  
    onClick={()=>{
      this.props.orders.addProductToCar(product);
    }}>+</Button>
*/