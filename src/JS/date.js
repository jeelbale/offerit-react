export const formatDateFromServer = (date, format = "dd/mm/aaaa")=> {
  format = format.toLowerCase();

  const systemDate = new Date(parseInt(date));
   
  if(format === "dd/mm/aaaa")
    return `${systemDate.getDate()}-${systemDate.getMonth()+1}-${systemDate.getFullYear()}`;

  if(format === "aaaa/mm/dd")
    return `${systemDate.getFullYear()}-${systemDate.getMonth()+1}-${systemDate.getDate()}`;
  
  else
    return `${systemDate.getMonth()+1}-${systemDate.getDate()}-${systemDate.getFullYear()}`;
}



 export const timeAgo = (time)=>{
 	try
      {
      		const moment = require("moment");
      		const date = moment(time).fromNow();

      		return date;

  		}catch(e){
  			alert("You need to import moment.js to use this function");
  			console.error(e);
  		}
     	return null
    }