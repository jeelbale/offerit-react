import React, { Component } from 'react';
import styled , { 
  keyframes, 
  css, 
  ThemeProvider } from 'styled-components';

import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';

const _ = require('lodash');
//Code
const CARD_HEIGHT= "430px"
//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 50%;

    position: relative;
    margin: 3rem auto;
    
    padding-bottom: 50px;
    text-align: left;
    border-bottom: solid 1px rgba(0,0,0,0.2);
     @media (max-width: 700px) {
      margin-top:3px;
       width: 90%;
    }
 
    
  }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 400;
  font-size: 20px;
  width: 100%;
  color:#202124;
  text-align: center;
  line-height: 1;
  @media (max-width: 700px) {
    font-size: 14px;
  }

`;

 const Subtitle = styled.h2`
   width: 100%;
    font-weight: 400;
    font-size: 18px;
    color: #5F6469;
    opacity: 0.77;
    text-align: center;
`;


 const ImgCircle = styled.div`
  display: none;
  width: 250px;
  height: 250px;
  border-radius: 50%;
  overflow: hidden;
  img{
    width: 145%;
  }
  @media (max-width: 700px) 
  {
   display: inline-block;
  }
`;
 const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  text-align: left;
  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : "black")};
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  border:0;
`;
 const Label = styled.label`
  font-size: 16px;
  text-align: center;
  font-weight: 500;
  a{
      color: #444444;
  }
  margin: 0 auto;
  margin-top: 2rem;
`;

 

const Card = styled.div`
  width: 50%;
  margin:2rem auto;
  background: white;
  border-radius: 10px;
  box-shadow:   0 0.3px 5px rgba(0,0,0,0.2);
  dborder: 1px solid rgba(0,0,0,0.4);
  text-align: center;
  padding: 2rem;
   ${props=>props.theme.utils.centerContent()}
    @media (max-width: 700px) {
  width: 100%;
  margin: 1rem 0;
  }
  
`;

 const Row = styled.div`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}
    @media (max-width: 700px) {
    display: block;
  }

`;



 const ImgContainer = styled.div`
    width: 25%;
    height: 100vh;
    margin: 0 auto;
    overflow: hidden;
    text-align:center;
    img{
    
     margin: 0 auto;
    }

        @media (max-width: 700px) {
    display: none;
  }
`;

 const Img = styled.img`
    height: 100%;
   ${props=>props.theme.utils.rowContent()}
    @media (max-width: 700px) {
    display: block;
  }

`;

class App extends Component
{
  constructor(){
    super();
    this.state = {
      register: false
    }
  }
  
  authWithGoogle() {
    
  }

  componentWillMount() {
    
  }
  render() {

    return (
      <React.Fragment>
   
       
       <Row>
       <ImgContainer>
       <Img src={require("static/imgs/frutas-verduras-720x540.jpg")} />
       </ImgContainer>


        <Container>
        <Card>

        <Title>
        Frutas, verduras y más.


        </Title>
        <Subtitle>
        Podras hacer todo tu mercado de manera comoda sin salir de dónde estás.
        </Subtitle>
        
        </Card>


        <Card>

        <Title>
        Solicita tu pedido


        </Title>
        <Subtitle>
       Puedes levantar tu orden desde tu dispositivo.
        </Subtitle>
        
        </Card>

     
             </Container>
        
           <ImgContainer>
       <Img src={require("static/imgs/verduras.jpg")} />
       </ImgContainer>

          </Row>
   
      </React.Fragment>
    );
  }
}



export default App;