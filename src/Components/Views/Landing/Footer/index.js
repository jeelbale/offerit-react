import React, { Component } from 'react';
import styled , { 
  keyframes, 
  css, 
  ThemeProvider } from 'styled-components';
 
import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';

const _ = require('lodash');
//Code
const CARD_HEIGHT= "430px"
//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 70%;
    position: relative;
    margin: 0 auto;
    padding: 50px 0;
    text-align: left;
    margin-top: 100px;  
    @media (max-width: 700px) {
    width: 90%;
    margin: 2rem auto;
    }  
`;

const TextContainer = styled.div`
    width: 30%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    @media (max-width: 700px) {
    width: 100%;
    margin: 2rem 0;
    }  
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 400;
  font-size: 22px;
  color:#202124;
  text-align: left;
  line-height: 1;
  @media (max-width: 700px) {
    font-size: 24px;
  }
`;

 const Subtitle = styled.h2`
  margin:12px 0;
    width: 100%;
    font-weight: 400;
    font-size: 16px;
    color: #5F6469;
    text-align: left;
    text-decoration: none;
    a{
       color: #5F6469;;
    }
`;

 const Row = styled.div`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}
   @media (max-width: 700px) {
   display: block;
    }  
`;

class App extends Component
{
  constructor(){
    super();
    this.state = {
      register: false
    }
  }
  
  authWithGoogle() {
    
  }

  componentWillMount() {
    
  }
  render() {

    return (
        <React.Fragment>


          <div className="mapouter">
            <div className="gmap_canvas">
            <iframe 
            width="100%" 
            height="500" 
            id="gmap_canvas" 
            src="https://maps.google.com/maps?q=Pi%C3%B1a%201724%2C%20local%2012-C%2C%20Mercado%20de%20Abastos%2C%20Guadalajara.&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
            </iframe>
            </div>
            <style>{`.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}`}</style></div>




          <Container>

           




            <Row> 
        
            <TextContainer >
            <Title>
           Raya frutas y verduras
            </Title>

            <Subtitle>
            En Raya frutas y verduras estamos comprometidos con la calidad, servicio y eficiencia para con nuestros clientes. Estamos para servirles siempre, ¡basta con una llamada!.
            </Subtitle>
           
          
         
            </TextContainer>     

             <TextContainer >
            <Title>
           ¡Acércate!


            </Title>

                   <Subtitle>


            Sé parte de los clientes exclusivos, envia tu petición de alta a: <br/><b>admin@rayafrutasyverduras.com</b>
              

            </Subtitle>

            <Subtitle>
            <a 
             style={{textDecoration: "none"}}
            href="/#inicio">
            Piña 1724, local 12-C, Mercado de Abastos, Guadalajara.
            </a>
            </Subtitle>


             <Subtitle>
              <a 
               style={{textDecoration: "none"}}
              href="/#como">
            (33) 3671-0942
            </a>
            </Subtitle>
             <Subtitle>
             <a 
              style={{textDecoration: "none"}}
             href="/#cobro">
            (33) 2264-7634
            </a>
            </Subtitle>
             <Subtitle>
             <a 
              style={{textDecoration: "none"}}
             href="/#ASK">
            (33) 3849-3439
            </a>
            </Subtitle>
             <Subtitle>
             <a 
              style={{textDecoration: "none"}}
             href="/#ASK">
            (33) 3671-3754
            </a>
            </Subtitle>
         
            </TextContainer>  


             <TextContainer >
            <Title>
            Legal
            </Title>
             <Link 
              style={{textDecoration: "none"}}
             to={"/privacidad"}> 
            <Subtitle>
           Privacidad 
            </Subtitle>
            </Link>
             <Link 
             style={{textDecoration: "none"}}
             to={"/terminos"}>
             <Subtitle>

             Términos y condiciones
            </Subtitle>
          </Link>
         
            </TextContainer>  
            </Row>









            </Container>
        
        </React.Fragment>
    );
  }
}



export default App;