import React, {useState} from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import OutlineEdit from "react-md-icon/dist/OutlineEdit";
import BaselineAccountBalanceWallet from "react-md-icon/dist/BaselineAccountBalanceWallet";
import RoundMenuIcon from "react-md-icon/dist/RoundMenu";
import { Link, withRouter} from 'react-router-dom';
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import MorePopover from "./MorePopover"


const Input = styled.input`
width: 300px;
margin: 0 auto;
  border: none;
  background: #f1f3f4;
  //border-bottom: 1px solid rgba(0,0,0,0.3);
  color: rgba(0,0,0,0.5);
  padding: 0.6rem 0.8rem;
  font-size: 14px;
   box-shadow: 0 0.3px 3px rgba(0,0,0,0.2);
   border-radius:17px;
  &:focus {
    outline: none !important;
    border:1px solid ${props=>props.theme.color.orange};
    
    }

`;

const LogoTitle = styled.div`
  color: ${(props)=>props.theme.color.orange};
  font-size: 22px;  
`;

 const Logo = styled.img`
  padding-top:3px;
  width: 30px;
  margin-left: -10px;
  //background: red;
`;

 const Nav = styled.nav`
 
  overflow: hidden;
  width: 100%;
  background: gray;
  // border-bottom: 1px solid rgba(0,0,0,0.09);
  box-shadow: 0 0.5px 6px rgba(0,0,0,0.2);
  height: 40px;
margin-bottom: 20px;
  color: ${props=>props.theme.color.navbarText};
  ${props=>props.theme.utils.rowContent()}

`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent()}
    width: 85.1%;
    margin: 0 auto;
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;

    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;

`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Item = styled.div`
    position: relative; 
    height: 40px;
    cursor: pointer;   
    color: white;
    padding:0 1rem;
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 0.5rem;"}
    ${props=>props.hideBig && `
      display: none;
      @media (max-width: 700px)
      {
          ${props.theme.utils.centerContent()}
      }
    `}
    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `}

   
    a{
        text-decoration: none;
      color: white;
 
       font-weight: 500;
    }

    ${props=>props.active && `background: rgba(0,0,0,0.4);`}
`;
 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;
const Icon = styled.i`
  vertical-align: bottom;
`;


const TabSet = styled.div`
display: ${props=>props.notNavbar?"none": "contents"};
@media (max-width: 700px) {
  display: none;
}
`;




const TabItem = ({children, active, onClick})=>{
      
  const TabSelector = styled.div`
    background: ${(props)=>props.theme.color.orange};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 16px;

    
    border-radius: 5px 5px 0 0;
    bottom: 0;
    margin:0 auto;
`;

  return (
    <Item 
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer
      >
       {children}
        {active &&  <TabSelector/>}
    </Item>
    )
}


const logo = require("static/logo.png")


const Toolbar = (props)=> {
    
 
  const [filter, changeFilter]  = useState("")
    return (  
      <React.Fragment>  
      <Nav>
        <NavContent>
        <AlignStart>
     
   
         <Item margin 

          active={props.location.pathname === "/profile/" + props.match.params.name}>
         <Link to={"/profile/" + props.match.params.name}>
        Ads 
        </Link>
        </Item>

         <Item margin
         active={props.location.pathname === "/profile/" + props.match.params.name + "/followers"}

         ><Link to={ "/profile/" + props.match.params.name + "/followers"}>
        Followers</Link> 
        </Item>

         <Item 
         active={props.location.pathname === "/profile/" + props.match.params.name + "/following"}
         margin><Link to={"/profile/" + props.match.params.name + "/following"}>
        Following </Link>
        </Item>


        </AlignStart>
{/* 
        <AlignEnd> 





       <Item  active={props.location.pathname === "/profile/" + props.match.params.name +"/wallet"}>
              <Link 
             
              to={"/profile/" + props.match.params.name +"/wallet"}>
                <BaselineAccountBalanceWallet
               style={{ 
                    fontSize:"30px",
                    margin: "0 1rem",
                    color:"white"
                  }}/>
                  </Link>
              </Item>
              <Item hide
                 active={props.location.pathname === "/profile/" + props.match.params.name +"/wallet"}
              >
              <Link 
           
              to={"/profile/" + props.match.params.name +"/wallet"}>
               Wallet
               </Link>
        </Item>
</AlignEnd>*/}
        
        </NavContent>
      </Nav>

      </React.Fragment>
  
    );
 }

export default withRouter(Toolbar);

/*
<AddItem />
  <Item>
  <MorePopover>
  <UserCirlce />
  </MorePopover>
  </Item>
*/