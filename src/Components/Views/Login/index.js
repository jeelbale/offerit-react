import React, { Component } from 'react';
import styled from 'styled-components';

import { Link, withRouter} from 'react-router-dom';
import firebase from "firebase"

import Loading from "Components/Molecules/Loading"
import {Input} from "Components/Utils/Inputs"

const _ = require('lodash');
const Login = withRouter((props)=> {

 const Input2= styled.input`
  width:100%;
  margin: 2rem auto;
  border: none;
  border-bottom: 1px solid rgba(0,0,0,0.2);
  color: rgba(0,0,0,0.2);
  padding: 1rem auto;
  font-size: 14px;
`;

const PseudofullScreen = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  position: fixed;
  top:0rem;
  left:0rem;
  right:0rem;
  bottom:0rem;
  width:100%;
  height:100%;
  background: transparent;
  padding: 0rem;
`;

 const FullScreen= styled.div`
  position: fixed;
 
  top:0rem;
  left:0rem;
  right:0rem;
  bottom:0rem;
  width:100%;
  height:100%;
  background: rgba(0,0,0,0.5);
  padding: 0rem;
   ${props=>props.theme.utils.centerContent()}
  z-index: 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;
`;

 const Card = styled.div`
  margin: 0 auto;
  border-radius: 3px;
  width: 25%;
  height: auto;
  color: rgba(0,0,0,0.8);
  background: white;
  position: relative;
  text-align: left;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.5);
  overflow: hidden;
   @media (max-width: 700px) {
    width: 90%; 
    height: 80vh 
  }
  
`;

 const NavBar = styled.div`
  position: relative;
  height: 96px;
  background: white;
  ${props=>props.theme.utils.centerContent()}
`;

const Title = styled.h2`
  margin: 1rem auto;
  font-weight: 500;
  text-align: center;
  font-size: 25px;
  color: #313033;
`;

const Subtitle = styled.h4`
  font-weight: 200;
  text-align: left;
  color: rgba(33,33,33,0.8);
  margin: 0.5rem 0;  
`;

 const NavTextContainer = styled.div`
  width: 90%;
  text-align: left;
`;

 const IconContainer = styled.div`
  position: absolute;
  left: 1rem;
  top: calc(50% - 1rem);
`;

 const Icon = styled.i`
  vertical-align: bottom;
  cursor:pointer;
`;

 const Container = styled.div`
  width:90%;
  margin: 2rem auto;
`;

 const ButtonContainer = styled.div`
  cursor: pointer;
  width: 100%;
  position: relative;
  margin-top: 1.2rem;
  text-align: right;

`;

 const Button = styled.button`

  position: relative;
  padding: 0.6rem 1rem;
  
  width: 100%;
  margin: 0.5rem auto;
  border-radius: 3px;
  overflow: hidden;

  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : "black")};
  cursor: pointer;
  font-weight: normal;
  font-size: 18px;
  border:0;

 
`;
const InputStyles = styled.input`
    padding: 0 1rem;
    margin: 0.5rem auto;
		width: 100%;
		height: 50px;
    font-size: 18px;
		border-radius: 5px;
		position: relative;

		background: white;
		color: #212121;
		border: 1px solid #E5E5E5;
	
`;

class Modal extends Component 
{
  constructor(props) 
  {
      
    super(props);
    this.state={
        username: "",
        password :"",
        error: null,
        loading: false,
    }
  }

   createComponent(form)
  {
        console.log(form)
  }
  
  logIn(){
    this.setState({loading: true})
    firebase.auth().signInWithEmailAndPassword(this.state.username + "@raya.com", this.state.password)
    .catch((error) =>{
    // Handle Errors here.
      this.setState({error, loading: false})
    // ...
    });
  }

  render()
  {

    const close = this.props.close//?this.props.close:this.props.hideOnClick;
    
  
     return(
      <FullScreen>
      <Link to="/">
      <PseudofullScreen onClick={close}/>
      </Link>

        <Card>
        
          <NavBar>
         
          <NavTextContainer>
            <Title>
             Acceder al mercado.
            </Title>
          </NavTextContainer>

          <label>Ingresa tus datos para continuar.</label>

          </NavBar>

          <Container>
          {this.state.error && 
              <label>{this.state.error.code} <br/> {this.state.error.message}</label>
          }

        <form>
            <Input 
            onChange={(e)=>{this.setState({username:e})}}
            value={this.state.username}
            placeholder="Ingresar el nombre de usuario"/>
            <br/>
            <Input 
            onChange={(e)=>{this.setState({password:e})}}
            value={this.state.password}
            type="password"
            placeholder="Ingresar la contraseña"/>
        </form>
            
        <br/>    
        <ButtonContainer>
          <Link to="/registrar">
              <Button style={{background: "white", color:"orange", "border":"1px solid orange"}}>Registrarme</Button>
          </Link>
          <Button 
              onClick={this.logIn.bind(this)}
              shadow>Acceder</Button>
        </ButtonContainer>

          </Container>
        </Card>
        {this.state.loading && <Loading text="Iniciando sesión..."/>}
      </FullScreen>
  );
  }
}


return <Modal {...props}/>
                
}
            )

export default Login
