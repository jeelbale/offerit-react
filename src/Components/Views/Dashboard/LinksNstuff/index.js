import React, { useContext, useState } from 'react'; 
import styled from 'styled-components';
 import {SteemContext} from 'react-steem-provider';
import {Link} from "react-router-dom"
const _ = require('lodash');

//Code
const CARD_HEIGHT= "430px"


//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Body = styled.div`
    width: 25%;
    position: relative;
      @media (max-width: 700px) 
  {
    width: 100%;
   
    display: none;
  }
   
`;

 const Container = styled.div`
    width: 90%;
    
    background: #fefefe;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  
    
    margin: 0px auto;
   
    min-height: 300px;
    padding: 2rem;
 
 
    padding-bottom: 50px;
    text-align: left;

        margin-bottom: 0.5rem;
  
    @media (max-width: 700px)
    {
         width: 100%;

         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 400;
  font-size: 18px;
  width: 90%;
  margin: 1rem auto;
  color:#202124;
  text-align: left;
  line-height: 1;

`;

const Subtitle = styled.h2`
  width: 90%;
  margin: 1rem auto;
  font-weight: 400;
  font-size: 14px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;

const ImgContainer = styled.div`
  width: 100%;
  height: 200px;
  margin: 0 auto;
  text-align:center;
  overflow: hidden;
  position: relative;
`;

const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;

const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  position: absolute;
  bottom:1rem;
  right: 1rem;
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  border:${props => (!props.transparent ? "none" : "1px solid "+ props.theme.color.orange)};
`;

const Card = styled.div`
  width: 90%;
  margin: 1.2rem auto;
  height: 240px;
  background: white;
  border-radius: 2px;

  text-align: center;

  overflow: hidden;
  position: relative;

  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }

`; 

 const Row = styled.div`
  width: 100%;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;

const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 22px;
  opacity: 0.9;
  text-align: left;
`;





const HOT = ["House in Sausalito", "Bike in NY"]

const Products = ({ category})=>{

    const {
    auth,
    actions
   
    } = useContext(SteemContext);




  return (
      <React.Fragment>
<Body>
        {auth && <Container>
        
                  <H1 >Accont</H1>
        
                  <Row>
                  {_.map([

{title:"My Ads",route:"/profile/"+ auth.name},
{title:"My Wallet",route:`/profile/${auth.name}/wallet`},
{title:"OfferIt blog",route:"/profile/offerit"}

 ],(data)=>(
        
                 
        
                      <Title key={data.title}>
                        <Link to={data.route} style={{textDecoration:"none", color:"black"}}>
                      {data.title}
                      </Link>
                      </Title>
        
        
                    ))  
                  }
                  </Row>
                </Container>}

            <Container>

          <H1>Hot on OfferIt</H1>

          <Row>
          {_.map(HOT,(title)=>(

         

              <Title key={title}>
              {title}
              </Title>


            ))  
          }
          </Row>
        </Container>



       </Body>
      </React.Fragment>
    );
}

export default Products;


/*

(!this.props.orders.value.products[product.id] && !this.props.disbled) &&
  <Button  
    onClick={()=>{
      this.props.orders.addProductToCar(product);
    }}>+</Button>
*/