import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import {ThemeProvider} from 'styled-components';
import theme from './Styles/theme';

//Settings
import "./Init"
 
import AuthProvider from "Components/Providers/Auth";
import SteemProvider from 'react-steem-provider';

//Components
import App from './App';

//SteemConnect Config JSON
const STEEM_CONFIG = {
        app:   'baronjensen', //'nctest', 
        callbackURL:  process.env.REACT_APP_STEEMCONNECT_REDIRECT_URL || "http://192.168.100.3:3000/", //https://offerit-9dc85.web.app/
        scope: [ 'login', 'vote', 
                'comment', 'delete_comment', 
                'comment_options', 'custom_json'
                ]
        };


ReactDOM.render(
	<SteemProvider config={STEEM_CONFIG}>
		<AuthProvider>
			<ThemeProvider theme={theme}>
				<App />
			</ThemeProvider>
		</AuthProvider>
	</SteemProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();