import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Link, Route, withRouter} from 'react-router-dom';

const _ = require('lodash');

const Container = styled.div`
  width: 90%;
  position: relative;
  margin: 0 auto;
  text-align: center;
`;

 const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  margin: 1rem 0.5rem;
  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? props.theme.color.green : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.green)};
  cursor: pointer;
  font-weight: normal;
  font-size: 18px;
  border: ${props => (!props.transparent ? "0" : `solid 1px ${props.theme.color.green}`)};;
`;

 const Label = styled.label`
  font-size: 16px;
  text-align: center;
  font-weight: 500;
  margin: 0 auto;
  margin-top: 2rem;
  a{
    color: #444444;
  }
`;

 const Body = styled.div`
  width: 100%;
  overflow: hidden;
  min-height: 100vh;
  position: relative;
  background: white;
  margin-top: 60px;
  @media (max-width: 700px) {
    padding-top: 10px; 
  }
  z-index: 1;
`;

const Row = styled.div`
  width: 90%;
  ${props=>props.theme.utils.rowContent()}
`;
 
class App extends Component{
  constructor(){
    super();
    this.state = {
      register: false
    }
  }
  render() {

    return (
      <React.Fragment>
      <Body>

      </Body>
      </React.Fragment>
    );
  }
}

export default withRouter(App)