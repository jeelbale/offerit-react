import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';

import LinksNstuff from "./LinksNstuff";
import Categories from "./Categories";
import PostList from "./PostList";

import {STATUS,  COLOR} from "const/status/"
import Navbar from "Components/Molecules/Navbar";
import Loading from "Components/Molecules/Loading";


const Row = styled.div`
  width: 90%;
  margin: 80px auto;
  ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    width: 100%;
    margin-top: 60px;
    margin-bottom: 0;
    display: block;
  }
`;

const Dashboard = ({computedMatch})=>{

  const [loading, setloading] = useState(false);
  const category = computedMatch.params.category;


   if(loading)  
    return (<Loading />);
  
  else
    return(
      <React.Fragment>
        <Navbar/>
        <Row>
        <Categories />
        <PostList category={category} />
        <LinksNstuff />
        </Row>
      </React.Fragment>
    );

}
export default Dashboard