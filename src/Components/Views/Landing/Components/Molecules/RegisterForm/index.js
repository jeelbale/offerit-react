import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Redirect, Link,BrowserRouter, Route, Switch, withRouter} from 'react-router-dom';

import Loading from "Components/Molecules/Loading"
import {Input} from "Components/Utils/Inputs"
import firebase from "firebase";


function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

const PseudofullScreen = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  position: fixed;
  top:0rem;
  left:0rem;
  right:0rem;
  bottom:0rem;
  width:100%;
  height:100%;
  background: transparent;
  padding: 0rem;
`;

 const FullScreen= styled.div`
  position: fixed;
  overflow-y: scroll;
  top:0rem;
  left:0rem;
  right:0rem;
  bottom:0rem;
  width:100%;
  height:100%;
  background: rgba(0,0,0,0.5);
  padding: 0rem;
  

  z-index: 999999999999;

`;

 const Card = styled.div`
  margin: 3rem auto;
  border-radius: 7px;
  width: 30%;
    padding: 0.8rem 0;
  color: rgba(0,0,0,0.8);
  background: white;
  position: relative;
  text-align: left;
  box-shadow: 0 0.5px 3px rgba(0,0,0,0.3);
  
   @media (max-width: 700px) {
    width: 90%;
    
  }
  
 
`;


 const NavBar = styled.div`
  position: relative;

  background: white;

  ${props=>props.theme.default.utils.centerContent}
`;



 const Title = styled.h2`
  margin: 0;
  font-weight: 500;
  text-align: center;
  font-size: 25px;
  color: #313033;
`;

 const Subtitle = styled.h4`
font-weight: 200;
text-align: left;
color: rgba(33,33,33,0.8);
margin: 0.5rem 0;  

`;

 const NavTextContainer = styled.div`
  width: 90%;
  text-align: left;
`;

 const IconContainer = styled.div`
  position: absolute;
  left: 1rem;
  top: calc(50% - 1rem);
`;

 const Icon = styled.i`
  vertical-align: bottom;
  cursor:pointer;
`;

 const Container = styled.div`
  width:90%;
  margin: 2rem auto;
`;

 const ButtonContainer = styled.div`
  cursor: pointer;
  width: 100%;
  position: relative;
  margin-top: 1.2rem;
  text-align: right;

`;

 const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  margin-right: 1rem;
  border-radius: 3px;
  overflow: hidden;

  background: ${props => (!props.transparent ? props.theme.colors.green : "transparent")};
  color: ${props => (!props.transparent ? "white" : "black")};
  cursor: pointer;
  font-weight: normal;
  font-size: 18px;
  border:0;

  ${props=>props.disabled && `  
  background:#676C72;
  cursor: no-allowed;
  `}
`;
     
const Tag = styled.label`
    position: absolute;
    color:#6200EE;
    font-size: 12px;
    top: -8px;
    left: 1rem;
`;

const InputStyles = styled.input`
    padding: 0 1rem;
    margin: 0.6rem auto;
    width: 100%;
    height: 50px;
    border-radius: 5px;
    position: relative;
    font-size: 18px;
    background: white;
    color: #212121;
    border: 1px solid #E5E5E5;
`;

const Body1 = styled.div`
  width:100%;
  position: relative;
  
`;




const RegisterForm = withRouter((props)=> {



class Modal extends Component 
{
  constructor(props) 
  {
    super(props);
    this.state={
        showConfirmation: false,
        loading: false,
        data:{
            name:"",
            apellido:"",
            mail:"",
            telefono:"",
            password:"",
            password2:""
        },
        ready: false,
    }
  }


    componentDidUpdate()
  { 
    const formValidation = this.validateForm()
    if(formValidation !== this.state.ready)
    {
      this.setState({ready: formValidation})
    }  
  }

  validateForm()
  {
        if(this.state.data.name
          && this.state.data.apellido
          && this.state.data.mail
          && this.state.data.telefono
          && this.state.data.password
          && this.state.data.password2
          && this.state.data.password2 === this.state.data.password
          && validateEmail(this.state.data.mail)
        )return true
  
    return false
  
  }
 
  async  createComponent()
  {
    this.setState({loading: true})
      if(this.state.data.password2 === this.state.data.password)
          firebase.auth().createUserWithEmailAndPassword(this.state.data.mail, this.state.data.password)
          .then(async (rest)=>{      
        await firebase.database().ref(`profile/${rest.user.uid}`).set(
          { 
                name: this.state.data.name,
                apellido: this.state.data.apellido,
                mail:this.state.data.mail,
                telefono:this.state.data.telefono,
                id: rest.user.uid
            })
            .catch((err)=>{
              this.setState({loading: false})
                alert("Hubo un error guardando los datos del perfil, porfavor verifique la configuracion antes de realizar una solicitud.")        
            }) 
          })
          .catch((error)  =>{
                this.setState({loading: false})
                var errorCode = error.code; //Verify code
                var errorMessage = error.message;
                 alert("Hubo un error al crear tu cuenta, intenta de nuevo. ")
            });   
  }
  render()
  {

    if(this.state.loading)
      return <Loading text="Creando cuenta..."/>
     return(
      <FullScreen>
      <Link to="/">
      <PseudofullScreen />
      </Link>
        <Card>
          <NavBar>
          <NavTextContainer>
          <br/>
            <Title>
              Regístrate para continuar
            </Title>
          </NavTextContainer>
          </NavBar>
          <Container>
          <div>
              <Input 
                value={this.state.data.name}
                onChange={(name)=>{this.setState({
                  data:{...this.state.data, name }  
                })}}
                placeholder="Nombre(s)"
                />
              
                <Input 
                  value={this.state.data.apellido}
                  onChange={(apellido)=>{
                    this.setState({data:{...this.state.data, apellido }})} 
                  }
                  placeholder="Apellidos"/>
              
              <Input 
                  value={this.state.data.telefono}
                  onChange={(telefono)=>{this.setState({data:{...this.state.data, telefono }  })} }
                  placeholder="Número de celular (10 digitos)"/>
              
              <Input 
                    type={"email"}
                  value={this.state.data.mail}
                  onChange={(mail)=>{this.setState({data:{...this.state.data, mail }  })} }
                  placeholder="Correo"/>
              
              <Input 
                  type={"password"}
                  value={this.state.data.password}
                  onChange={(password)=>{this.setState({data:{...this.state.data, password }  })} }
                  placeholder="Contraseña"/>

              <Input 
                  type={"password"}
                  value={this.state.data.password2}
                  onChange={(password2)=>{this.setState({data:{...this.state.data, password2 }  })} }
                  placeholder="Confirmar contraseña"/>          
          </div>

           <ButtonContainer>
            <Link to ="/">
              <Button transparent>Cancelar</Button>
            </Link>
            
            <Button
            disabled={!this.state.ready} 
            onClick={()=>{
              this.createComponent()
              this.setState({showConfirmation: false})} 
            }}
              shadow 
             >Continuar</Button>
              </ButtonContainer>
          </Container>
        </Card>
  
      </FullScreen>
  );
  }
}

return <Modal {...props}/>              
}
)

export default RegisterForm