import React, { Component } from 'react';
import styled from 'styled-components';
import { Redirect, Link, withRouter} from 'react-router-dom';
import RoundClose from "react-md-icon/dist/RoundClose";
import {Input} from "Components/Utils/Inputs"


 const PseudofullScreen = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  position: fixed;
  top:0rem;
  left:0rem;
  right:0rem;
  bottom:0rem;
  width:100%;
  height:100%;
  background: transparent;
  padding: 0rem;
`;

 const FullScreen= styled.div`
  position: fixed;
  overflow-y: scroll;
  top:0rem;
  left:0rem;
  right:0rem;
  bottom:0rem;
  width:100%;
  height:100%;
  background: rgba(0,0,0,0.8);
  padding: 0rem;
  padding-top:1%;
  z-index: 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;
    ${props=>props.theme.utils.centerContent()}
`;


 const Button1 = styled.button`
  position: relative;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  
  overflow: hidden;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  cursor: pointer;
  font-weight: normal;
  font-size: 14px;
  border:${props => (!props.transparent ? "none" : "1px solid "+ props.theme.color.orange)};
`;
 const Label = styled.label`
  font-size: 14px;
  text-align: center;
  width: 50px;
  color: rgba(0,0,0,0.9);
`;

const Card = styled.div`
  width: 40%;

  position: relative;
  background: white;
  border-radius: 2px;
  box-shadow: 0 0.3px 5px rgba(0,0,0,0.2);
  text-align: center;
  margin: 1.2rem 0;
  overflow: hidden;
  padding: 2rem ;
  ${props=>props.theme.utils.centerContent()}
  @media (max-width: 700px)
  {
    width: 100%;
    margin: 1rem 0;
  }
`;

 const RowButtons = styled.div`
  width: 60%;
  margin: 1rem auto;
  ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) {
    display: block;
  }
`;

 const NavBar = styled.div`
  position: absolute;
  top:0;
  left:0;
  width: 100%;
  height: 65px;
  background: white;
  box-shadow: 0 0.5px 10px rgba(0,0,0,0.3);
  ${props=>props.theme.utils.centerContent()}
`;


 const Title = styled.h2`
  margin: 0;
  font-weight: 400;
  text-align: center;
  font-size: 25px;
  color: #202124;
`;

 const Subtitle = styled.h4`
font-weight: 400;
text-align: center;
color: #5F6469;
font-size: 16px;
margin: 0.5rem 0;  

`;

 const NavTextContainer = styled.div`
  width: 90%;
  text-align: left;

`;

 const IconContainer = styled.div`
  position: absolute;
  left: 1rem;
  top: 1rem;
`;

 const Icon = styled.i`
  vertical-align: bottom;
  cursor:pointer;
`;

 const Img = styled.img`
width: 150px;
margin: 1rem auto;
text-align: center;

`;


 const Container = styled.div`
  width:80%;
  margin: 2rem auto;
   padding-top: 65px;
   text-align: center;
`;


 const ContainerBox = styled.div`
  width:100%;
  border: 1px gray solid;
  border-radius: 7px;
  margin: 2rem auto;
`;

 const ButtonContainer = styled.div`
  cursor: pointer;
  width: 100%;
  position: relative;
  margin-top: 2.3rem;
  text-align: center;

`;

 const Button = styled.button`

  position: relative;
  padding: 0.6rem 1rem;
  
  
  margin: 2rem 0;
  border-radius: 3px;
  overflow: hidden;

  background: ${props => props.theme.color.orange};
  color: white;
  cursor: pointer;
  font-weight: normal;
  font-size: 18px;
  border:0;

 
`;

 const Row = styled.div`
    width: 100%;
   
   ${props=>props.theme.utils.rowContent()  }
    @media (max-width: 700px) {
    display: block;
  }
`;

const _ = require('lodash');

const STATUS = [
"Su orden está en proceso.", 
"Su orden ha sido recibida.",
"Su orden está en camino.",
"Su orden ha sido entregada.",
"Su orden ha finalizado."]

class Modal extends Component 
{
  constructor(props) 
  {
    super(props);
    this.state={
     
      total: 10,
    }
  }




  render()
  {

    const close = this.props.close;

    
     return(
      <FullScreen>
      <PseudofullScreen onClick={close}/>
        <Card> <RoundClose
              onClick={close}
             
              style={{
                position: "absolute",
                color: "rgba(0,0,0,0.4)",
                cursor:"pointer",
                top:"0.5rem", 
                right:"0.5rem"
              }}/>

      
                <Title>
                {this.props.name}
                </Title>

                 <Subtitle>
                   {this.props.description}
                </Subtitle>
                <Subtitle>
                 ${this.props.price} / {this.props.units}
                </Subtitle>
               
             
              <RowButtons>
              <Button1 
              onClick={()=>{this.setState({total:this.state.total-1})}}
              transparent>-</Button1>
              <Label>
               <b> {this.state.total}</b> {this.props.units}
              </Label>
              <Button1   onClick={()=>{this.setState({total:this.state.total + 1})}}>+</Button1>
              </RowButtons>
             
             <Button onClick={this.props.add}>Agregar al carrito</Button>
          </Card>
        </FullScreen>
      );
    }
  }


export default withRouter(Modal)