import React, { Component } from 'react';
import { Redirect, Link, Route } from 'react-router-dom';
//import Landing from "Components/Views/Landing"


const Landing = ()=><h1>Landing page</h1>


 export const AuthenticatedRouteRedirect = ({component: Component, authenticated, ...rest}) =>
{
  return (
    <Route
      {...rest}
      render={(props) => authenticated?
        <Component  {...rest} />
          :<Redirect to="/" />}
      />)
}

 export const AuthenticatedRoute = ({component: Component, authenticated, ...rest}) =>
{
  
  return (
    <Route
      {...rest}
      render={(props) => authenticated?
        <Component   {...rest}  />
          :<Landing   {...rest} />}
      />)
}

 export const AuthenticatedRouteHidden = ({component: Component, authenticated, ...rest}) =>
{
  return (
    <Route
      {...rest}
      render={(props) => authenticated?
        <Component  {...rest} />
          :null}
      />)
}



 

