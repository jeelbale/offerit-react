import React, { Component } from 'react';
import styled , { 
  keyframes, 
  css, 
  ThemeProvider } from 'styled-components';

import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';

const _ = require('lodash');
//Code
const CARD_HEIGHT= "430px"
//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 90%;
    position: relative;
    margin: 2rem auto;
    padding-top: 100px;
    text-align: left;
    @media (max-width: 700px) {
        padding-top: 0px;
    width: 90%;
    margin: 2rem auto;
    }  
 

`;
 
const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 4rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 400;
  font-size: 40px;
  width: 100%;
  color:#202124;
  text-align: center;
  line-height: 1;
  @media (max-width: 700px) {
    font-size: 28px;
  }
`;

 const Subtitle = styled.h2`
   width: 100%;
    font-weight: 400;
    font-size: 17px;
    color: #5F6469;
    text-align: left;
`;


 const Div = styled.div`

  width: 25%;
  font-size: 30px;
  text-align: center;
  overflow: hidden;
 
    @media (max-width: 700px) {
   
  width: 100%;
  }

`;

 const ImgCircle = styled.div`

  width: 250px;
  height: 250px;
  border-radius: 50%;
  overflow: hidden;
  margin: 0 auto;
  box-shadow: 0 0.5px 3px rgba(0,0,0,0.3);

`;
const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  margin: 0 0.5rem;
  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  cursor: pointer;
  font-weight: normal;
  font-size: 14px;
  border: ${props => (!props.transparent ? "0" : `solid 1px ${props.theme.color.orange}`)};;
`;

 const Label = styled.label`
  font-size: 16px;
  text-align: center;
  font-weight: 500;
  a{
      color: #444444;
  }
  margin: 0 auto;
  margin-top: 2rem;
`;

 const Body = styled.div`
  width: 100%;
  overflow: hidden;
  min-height: 100vh;
  position: relative;
  background: #FAFAFA;
  margin-top: 100px;
  @media (max-width: 700px) {
    padding-top: 10px; 
  }
`;

 const RoudLabel = styled.div`
  display: inline-block;
  border-radius: 22px;
  background: white;
  color: ${props=>props.theme.color.orange};
  border: 1px solid ${props=>props.theme.color.orange};
  padding: 0.2rem 1rem;
  font-size: 19px;
  margin: 2rem 0;
`;

  const Card = styled.div`
    width: 50%;
   
   
    border-radius: 3px;
  
    text-align: left;
    padding: 1rem 0;


       @media (max-width: 700px) {
    width: 90%;
  }

`;

 const Row = styled.div`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}
`;
 const CardTitle = styled.h1`
    width: 100%;
    font-weight: 500;
    font-size: 26px;
    color: #3C4043;
    text-align: left;
    margin: 0.5rem auto;
`;
 const CardSubtitle = styled.h2`
    width: 100%;
    font-weight: 400;
    font-size: 22px;
    color: #5F6469;
    text-align: left;
    margin: 2rem auto;
      @media (max-width: 700px) {
    font-size: 20px;
  }

`;

 const More = styled.h2`
    width: 100%;
    font-weight: 500;
    font-size: 17px;
    color: #5F6469;
    margin: 150px 0;
    text-align: center;
    a{
      text-decoration: none;
      color: ${props=>props.theme.color.orange}
    }
`;


export const ImgContainer = styled.div`
  width: 20%;

   border-radius: 7px;
   overflow: hidden;

    @media (max-width: 700px) {
    width: 48%;
  }
`;

export const Img = styled.img`
  width: 100%;
  display: inline-block;
`;
const logo = require("static/imgs/truck.jpg")
const logo2 = require("static/imgs/chef.jpg")
class App extends Component
{
  constructor(){
    super();
    this.state = {
      register: false
    }
  }
  authWithGoogle() { 
  }
  componentWillMount() { 
  }
  render() {

    return (
      <React.Fragment>
  
        <Container>

 

        <Row>


        <Card>
          <Title>
       CREEMOS EN EL SABOR
        </Title> 
       
        <CardSubtitle>
    Directamente del campo a tu mesa, ese es nuestro compromiso, el SABOR y la calidad que esperas y que tu clientes también esperan, somos uno de los más grandes distribuidires, con más de <b>150 clientes </b>
    en la zona metropolitana de Guadalajara con sede en le Mercado de Abastos.
        </CardSubtitle>  
       

    
        </Card> 
<ImgContainer >
          <Img src={logo2}  />  
         </ImgContainer>
         <ImgContainer>
          <Img src={logo}  />  
          </ImgContainer>    
  </Row>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
      
      
<Row>
<Div>
<ImgCircle>
<Img style={{width:"310%"}} src={require("static/imgs/hotel.jpg")}  />
</ImgCircle>
<br/>
<br/>
Hoteles
</Div>


<Div>
<ImgCircle>
<Img style={{width:"300%"}}  src={require("static/imgs/clubdeportivo.jpg")}   />
</ImgCircle>
<br/>
<br/>
Restaurantes
</Div>



<Div>
<ImgCircle>
<Img style={{width:"300%"}} src={require("static/imgs/hospital.jpg")}  />
</ImgCircle>
<br/>
<br/>
Hospitales
</Div>




</Row>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/> 

        </Container>
        <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       
      </React.Fragment>
    );
  }
}

export default App;