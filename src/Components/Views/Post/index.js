import React, { useState, useEffect, useContext } from 'react';
import LinksNstuff from "./LinksNstuff";
import Comments from "./Comments";
import PostBody from "./PostBody";
import styled from 'styled-components';
import PlayIcon from "react-md-icon/dist/RoundPlayCircleFilled";
import {STATUS,  COLOR} from "const/status/"
import Navbar from "Components/Molecules/Navbar";
import Loading from "Components/Molecules/Loading";
//import OrderModal from "Components/Molecules/OrderModal"
import firebase from "firebase";
import Rebase from "re-base";
import SteemAsync from 'react-steem-provider/SteemAsync';


const Row = styled.div`
  width: 90%;
  margin: 80px auto;
  ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
  display: block;
  }
`;

const Post = ({computedMatch})=>{
  
const [loading, setloading] = useState(true);
const [post, setPost] = useState({});
 

 useEffect(()=>{

    SteemAsync.getPost(computedMatch.params.author, computedMatch.params.permlink).then(
    (response)=>{

     setPost(response)

    console.log("post:",response);

   //  console.log(response.active_votes.length);


     setloading(false)

   }).catch(
    (err)=>console.error(err)) 

  },[])

  if(loading)  
    return (<Loading />);
  else 
    return(
      <React.Fragment>
        <Navbar/>


        <PostBody 
        post={post}
        id={0}/>


        <Comments
        author={computedMatch.params.author}
        permlink={computedMatch.params.permlink} 
        id={0} />

        
      </React.Fragment>
    );
}
export default Post