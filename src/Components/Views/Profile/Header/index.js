import React, { useEffect, useState, useContext } from 'react'; 
import styled from 'styled-components';

import SteemAsync from 'react-steem-provider/SteemAsync';
import {parserSteemRep, parserSteemSimpleRep} from 'react-steem-provider/Helpers';
import Loading from "Components/Molecules/Loading";
import {SteemContext} from 'react-steem-provider';

const _ = require('lodash');

//Code
const CARD_HEIGHT= "430px"


//Icons



 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Body = styled.div`
    width: 100%;
    position: relative;
   background: rgba(0,0,0,0.6);
    height: 350px;
    margin: 0;
    margin-top:60px;

    overflow: hidden;
     @media (max-width: 700px)
    {
         height: auto;
         
    }
    

`;

 const Container = styled.div`
    width: 50%;
    ${props=>props.theme.utils.centerContent()}

    border-radius: 5px;
  
  
    
    margin: 0px auto;
   
   width: 100%;
    padding: 2rem;
 
 
    padding-bottom: 50px;
    text-align: left;
  
    @media (max-width: 700px)
    {
         width: 100%;
         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 28px;
  margin: 1rem auto;
  color:white;
  text-align: left;
  line-height: 1;
`;

const Subtitle = styled.h2`

  margin: 0.3rem auto;
  width: 60%;
  font-weight: 400;
  font-size: 14px;
  color: white;
  
  text-align: center;
   @media (max-width: 700px)
    {
         width: 90%;
         
    }


`;

const ImgContainer = styled.div`
  width: 100%;
  height: 200px;
  margin: 0 auto;
  text-align:center;
  overflow: hidden;
  position: relative;
`;



const Button = styled.button`
  position: absolute;
  padding: 0.6rem 1rem;
  border-radius: 7px;
width: 150px;

  top:1rem;
  right: 1rem;
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.disbled ? "#879be4" : "gray")};
  color: ${props => (!props.transparent ? "white" : "#879be4")};
  border:${props => (!props.transparent ? "none" : "1px solid "+ "#879be4")};

   @media (max-width: 700px)
    {
        width: 80px;
        font-size: 11px;

         
    }
`;



const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;


const ImgCircleContainer = styled.div`  
  border:1px solid rgba(0,0,0,0.4);
  width: 60px;
  height:60px;
  border-radius: 50%;
overflow: hidden;
background: red;



  
`;


const Rows = styled.div`
  width: 90%;
  margin: 0 auto;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;

const Img = styled.img`
  width: 100%;
   @media (max-width: 700px)
    {
         width: auto;
         height: 100%;
         
    }
 
`;

const ImgCover = styled.div`
      position: absolute;
      top: 0px;
      left: 0;
      right: 0;
      z-index: -1;
      background: rgba(0,0,0,0.8);
      opacity: 0.8;
      overflow: hidden;
    @media (max-width: 700px)
    {
         height: 100%;
         
    }
 
`;
const ImgCovexxr = styled.div`
      position: absolute;
      top: 0px;
      left: 0;
      right: 0;
      z-index: 1;
      background: rgba(0,0,0,0.8);
      background: black;
`;


const Header = ({ name})=>{

      const {
    auth,
    actions
    } = useContext(SteemContext);

  // const profile = {...fakeProfiles[name], json_metadata:JSON.parse(fakeProfiles[name].json_metadata)}
  // console.log("Prodisadsadle",profile);

  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);
  const [follow, setFollow] = useState(false);
  const [disabeFollow, disabeFollowFollow] = useState(false);

  useEffect(()=>{

   SteemAsync.getUserAccount(name).then(
    (user)=>{ 
       console.log("user ",user)
       var json_metadata = {}
        
    try 
     { 
        json_metadata = JSON.parse(user[0].json_metadata);
     }catch(e){

     }

    setUser({...user[0], json_metadata})
    setLoading(false); 

    if(auth)
    SteemAsync.getFollowing(auth.name,1, name).then(
    (response)=>{ 
        

    if(response.length > 0)
      if(response[0].following === name)
          setFollow(true)
      

    /*
      SteemAsync.getFollowCount(username).then(
    (response)=>{ console.log(response) }).catch(
    (err)=>console.error(err));
    */
    
    }).catch(
    (err)=>console.error(err));
    

    }).catch((err)=>{ 

      setLoading(false); 
      console.error(err);

    });
  },[])


  if(loading || !user)
    return <Loading />
  return (
      <React.Fragment>
<Body>
{user.json_metadata.profile && 
<ImgCover><Img src={user.json_metadata.profile.cover_image}/></ImgCover>}

<Container>
<ImgCircleContainer>
{user.json_metadata.profile && <Img src={user.json_metadata.profile.profile_image}/>}
</ImgCircleContainer>
{user.json_metadata.profile && 
<Title>
{name} <i style={{fontWeight:"400", fontSize:"22px"}}>({parserSteemRep(user.reputation)})</i>
</Title>}

{user.json_metadata.profile && 
<React.Fragment>
  
  {user.json_metadata.profile.website && 
    <Subtitle><b>Website:</b><br/> {user.json_metadata.profile.website}</Subtitle>}
  {user.json_metadata.profile.location &&
  <Subtitle><b>Location:</b><br/> {user.json_metadata.profile.location}</Subtitle>}

  {user.json_metadata.profile.about &&
  <Subtitle><b>About:</b><br/>{user.json_metadata.profile.about}</Subtitle>}

</React.Fragment>
}

</Container>

{auth && <React.Fragment>
<Button 
disbled={disabeFollow}
  onClick={()=>{
  disabeFollowFollow(true)
  if(!follow)
  actions.follow(name).then((r)=>{
    setFollow(true)
    disabeFollowFollow(false)

  }).catch((e)=>{alert("Error following");
   disabeFollowFollow(false)
})
  else
  actions.unfollow(auth.name,name).then(()=>{

    setFollow(false);
     disabeFollowFollow(false);
  }).catch(()=>{ 
    alert("Error unfollowing");
     disabeFollowFollow(false);
  })
}}>{follow?"Unfollow":"Follow"}{disabeFollow && "..."}</Button>
{/*<Button transparent style={{top: "60px"}}>Mute </Button>*/}
</React.Fragment>}
       </Body>
      </React.Fragment>
    );
}

export default Header;


/*

(!this.props.orders.value.products[product.id] && !this.props.disbled) &&
  <Button  
    onClick={()=>{
      this.props.orders.addProductToCar(product);
    }}>+</Button>
*/