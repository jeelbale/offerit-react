export const STATUS = [
"Su orden está en proceso. 🕑", 
"Su orden ha sido recibida. 👁️‍🗨️🤗",
"Su orden está en camino. 🚐",
"Su orden ha sido entregada. ✔️✔️✔️",
"La orden ha finalizado. 🏁👋"]

export const COLOR = [
"tomato", 
"orange",
"blue",
"green",
"gray"]

