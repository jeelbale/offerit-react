import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import ReactMarkdown from 'react-markdown';
import {Link} from "react-router-dom";
import {parseSteemMarkdown} from 'react-steem-provider/Helpers';
import {parserSteemRep, parserSteemSimpleRep} from 'react-steem-provider/Helpers';

import {timeAgo} from 'JS/date';
import BaselineKeyboardArrowUp from "react-md-icon/dist/BaselineKeyboardArrowUp";
import BaselineKeyboardArrowDown from "react-md-icon/dist/BaselineKeyboardArrowDown";
import BaselineComment from "react-md-icon/dist/BaselineComment";
import SteemAsync from 'react-steem-provider/SteemAsync';
import {SteemContext} from 'react-steem-provider';
const _ = require('lodash');

//Code
const CARD_HEIGHT= "430px"

//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 70%;
    background: #fefefe;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
    left: 20%;
    
    
    overflow: hidden;

    min-height: 800px;
    padding: 2rem 0;
 
 
    padding-bottom: 50px;
    text-align: left;
    margin: 80px auto;
    margin-bottom: 1rem;
    @media (max-width: 700px)
    {
         width: 100%;
          padding: 2rem 0;
          margin: 60px auto;
         
    }
`;
 const Content = styled.div`
    width: 90%;
    overflow: hidden;
    margin: 0 auto;
   
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 30px;
  width: 90%;
  margin: 1rem auto;
  color:#202124;
  text-align: left;
  line-height: 1;

   @media (max-width: 700px)
    {
        
  font-size: 22px;
         
    }

`;

const Title2 = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 25px;
  width: 90%;
  margin: 1rem auto;
  color:#202124;
  text-align: left;
  line-height: 1;

   @media (max-width: 700px)
    {
        
  font-size: 12px;
         
    }

`;

const Subtitle = styled.h2`
  
  margin: 1rem auto;
  font-weight: 400;
  font-size: 16px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
   @media (max-width: 700px)
    {
        
  font-size: 12px;
         
    }
`;

const ImgContainer = styled.div`
  width: 100%;
  height: 200px;
  margin: 0 auto;
  text-align:center;
  overflow: hidden;
  position: relative;
`;

const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  position: absolute;
  bottom:1rem;
  right: 1rem;
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  border:${props => (!props.transparent ? "none" : "1px solid "+ props.theme.color.orange)};
`;

const Card = styled.div`
  width: 90%;
  margin: 1.2rem auto;
  height: 240px;
  background: white;
  border-radius: 2px;

  text-align: center;

  overflow: hidden;
  position: relative;

  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }

`;




const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 22px;
  opacity: 0.9;
  text-align: left;
`;


const Item = styled.div`

  margin: 0 0.5rem;
  height: 60px;

  ${props=>props.theme.utils.centerContent()}
   @media (max-width: 700px) 
  {
   ${props=>props.hide && `display: none;`}
  }
 
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;

    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;

`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;


const Divider = styled.div`  
  width:1px;
  background: rgba(0,0,0,0.1);
  height: 30%;

`;
const Circle = styled.div`  
  border:1px solid rgba(0,0,2,0.3);
  cursor: pointer;
  &:hover{
    background: rgba(0,0,0,0.4);
  }
  width: 20px;
  height:20px;
  border-radius: 50%;
   ${props=>props.theme.utils.centerContent()}

  ${props=>props.voted && ` 
     background: #b79ee6;
   color: white;`}


${props=>props.load && `background: green;`}

`;


const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;


const ImgCircleContainer = styled.div`  
  border:1px solid rgba(0,0,2,0.2);
  width: 30px;
  height:30px;
  border-radius: 50%;
overflow: hidden;
background: purple;
opacity: 0.6;
  
`;




const Row = styled.div`
  width: 90%;
  margin: 0 auto;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;



const Markdown = styled.div`
  img{
    max-width: 100%;
  }


`;

const Img = styled.img`
  width: 100%;
 
`;



const Tag = styled.div`
  padding: 0.8rem 1.2rem;
  background: rgba(0,0,0,0.1);
  border: 1px solid rgba(0,0,0,0.2);
  color: rgba(0,0,0,0.5);
  border-radius: 7px;

    @media (max-width: 700px) 
  {
    font-size: 9px;
    padding: 0.1rem .2rem;
  }
 
`;


const PostBody = ({ post})=>{

    const [author, setAuthor] = useState("");
    const [json_metadata, setJson] = useState(false);
     const [voted, setVote] = useState(0);
  const [loadVote, setLoadVote] = useState(false);

  

    const {
      auth,
      actions,
      steemConnect
    } = useContext(SteemContext);


 


    const Vote = (w = 1000)=>{

      if(auth)
     { setLoadVote(true)
           steemConnect.vote(auth.name, post.author, post.permlink, w, (err, res)=> {
         if(err)
           alert("Error voting");
         else
           {
             setVote(voted + w);
     
           }
     
           setLoadVote(false)
     
     
         });}
    }

    useEffect(()=>{


if(auth)
    {const myvote = post.active_votes.filter((a)=>a.voter === auth.name)
    
        const votes = myvote.map((vote)=> parseFloat(vote.percent))
        const add =  votes.reduce((a, b) => a+b, 0);
    
    
    
         setVote(add);}

    },[])


   useEffect(()=>{
    SteemAsync.getUserAccount(post.author).then(
    (response)=>{
      try{
        const a = JSON.parse(response[0]["json_metadata"]).profile.profile_image; 
        setAuthor(a)       
      }
      catch(e){
        
      }
       //setloading(false)
     }).catch(
      (err)=>console.error(err)) 

    },[])

   
 try
  { 
    post["json_metadata"]= JSON.parse(post.json_metadata);
    setJson(true)
    //console.log(post["json_metadata"])
  }

  catch(e){}


  //console.log(products)
  return (
      <React.Fragment>

        <Container>
        <Content>
           
          <Title>{post.title}</Title>

          <br/>

          <Row>
              <AlignStart>

              <Item style={{width:"38px"}}>
             {author && <ImgCircleContainer><Img src={author} /></ImgCircleContainer>}
              </Item>
              <Item> 
              <Link to={"/profile/"+post.author} style={{textDecoration:"none"}}>   
                <Title2>
                {post.author}
                </Title2>
              </Link>
              </Item>
              <Item hide> 
              <Link to={"/profile/"+post.author} style={{textDecoration:"none"}}>   
                ({parserSteemRep(post.author_reputation)})
              </Link>
              </Item>

              
              <Item ><Link to={"/dashboard/"+post.category} style={{textDecoration:"none"}}><Subtitle>in {post.category}</Subtitle>  </Link></Item>
            
              <Item hide>
                  <Subtitle> -  {timeAgo(post.created)}</Subtitle>
              
              </Item>

              </AlignStart>

              </Row>
              <hr style={{opacity:"0.4"}}/>
              <br/>
              <br/>

         
           <Markdown 
               
              dangerouslySetInnerHTML={{ __html: parseSteemMarkdown(post.body) }} />
          </Content>
          <br/>
           <br/>
            <br/>
             <br/>
<hr/>
          {json_metadata &&  <Row>
              <AlignStart>
             { _.map(post.json_metadata.tags,(tag)=><Item style={{marginRight:"1rem"}}
              key={tag}>
              <Tag>{tag}</Tag>
                           </Item>) }
              </AlignStart>
              </Row>
            }



              <Row>
              <AlignStart>


             {loadVote || !auth?<Item>{!auth?"":"Loading..."}</Item>:<React.Fragment> <Item>
                           <Circle  
                             load={loadVote}
                           voted={voted > 0} onClick={()=>{
                             if(voted <= 0)
                             Vote()}}>
                           <BaselineKeyboardArrowUp/></Circle></Item>
                           <Item>

                           <Circle 
                              load={loadVote}
                              voted={voted < 0}
                              onClick={()=>{
                                if(voted >= 0)
                                  Vote(-1000)
                              }}>
             
                           <BaselineKeyboardArrowDown/>
                          </Circle></Item></React.Fragment>}



              <Item>${post.pending_payout_value}</Item>
              <Item >  <Divider /> </Item>
              <Item><BaselineKeyboardArrowUp/></Item>
              <Item > {post.active_votes.length} </Item>
              <Item >  <Divider /> </Item>
              <Item> <Link to={`/post/${post.author}/${post.permlink}#comments`}><BaselineComment/></Link></Item> 
              <Item> {post.children} </Item>
              </AlignStart>
              </Row>
        </Container>
      </React.Fragment>
    );
}

export default PostBody;
