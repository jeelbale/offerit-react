import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';

import LinksNstuff from "./LinksNstuff";
import Header from "./Header";
import PostList from "./PostList";
import FollowList from "./FollowList";
import Wallet from "./Wallet";

import {STATUS,  COLOR} from "const/status/"
import Navbar from "Components/Molecules/Navbar";
import SubNavbar from "./Navbar";
import Loading from "Components/Molecules/Loading";

import {Route, Switch, Redirect } from "react-router-dom";



 const Row = styled.div`
  width: 100%;
  margin: 80px auto;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;


const Profile = ({computedMatch})=>{

  const [loading, setloading] = useState(false);



  const name = computedMatch.params.name;



   if(loading)  
    return (<Loading />);
  else
    return(
      <React.Fragment>

        <Navbar/>
        
        <Header name={name} />
        <div style={{background: "white", width:"100%"}}>
        <SubNavbar/>

<Switch>

        <Route
            exact
              path={"/profile/" + name +"/following"}
              component={(props) => { 
              return ( <FollowList {...props} name={name}/>)  
              }}   
            />

            <Route
                exact
              path={"/profile/" + name +"/followers"}
              component={(props) => { 
              return ( <FollowList {...props} name={name} followers={true}/>)  
              }}   
            />


        <Route
            exact
              path={"/profile/" + name +"/wallet"}
              component={(props) => { 
              return ( <Wallet {...props} name={name} />)  
              }}   
            />

        <Route
            exact
              path={"/profile/" + name }
              component={(props) => { 
              return ( <PostList {...props} name={name}/>)  
              }}   
            />
      </Switch>

       
        </div>
       

   
      </React.Fragment>
    );

}
export default Profile