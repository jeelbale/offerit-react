import React, {Component, Fragment} from 'react';
import { Container, PseudoContainer, Content, Description, ImageContainer, Image, Information, Name, Email, AccountIcon, ExploreIcon, SettingsIcon, ExitIcon} from './elements.js';
import BaselineAccountCircle from "react-md-icon/dist/BaselineAccountCircle";
import BaselineHistory from "react-md-icon/dist/BaselineHistory";
import ListItem from './ListOptions/ListItem/';
import LineBreak from './ListOptions/LineBreak/';

class ProfileOptions extends Component {
	render() {
		return(
			<Fragment>
				
				<Content>


					<ListItem
					style= {{padding:"0"}}
					 link="/dashboard" 
					icon={null} description="Mercado"/>
							
			
<LineBreak/>
					<ListItem
					style= {{padding:"0"}}
					 link="/dashboard/history" 
					icon={null} description="Historial de pedidos"/>
							
				
			<LineBreak/>	

					
					<ListItem link="/logout" icon={null} 
					description="Cerrar sesión"/>
				</Content>
			</Fragment>
		)
	}
}

export default ProfileOptions;
