import React, { useEffect, useState, useContext } from 'react'; 
import styled from 'styled-components';
import { Redirect, Link, withRouter} from 'react-router-dom';
import RoundClose from "react-md-icon/dist/RoundClose";
import {Input} from "Components/Utils/Inputs"
 import SteemAsync from 'react-steem-provider/SteemAsync';
import {parserSteemRep, parserSteemSimpleRep} from 'react-steem-provider/Helpers';
import Loading from "Components/Molecules/Loading";
import {SteemContext} from 'react-steem-provider';


const uuid4 = require('uuid/v4');

const Card = styled.div`
  width: 70%;
  min-height: 500px;
  position: relative;
  background: white;
  border-radius: 2px;
  box-shadow: 0 0.3px 5px rgba(0,0,0,0.2);
  text-align: center;
  margin: 80px auto;
 
  overflow: hidden;
  padding: 2rem ;

  @media (max-width: 700px)
  {
    width: 100%;
    margin: 1rem 0;
  }
`;

 const RowButtons = styled.div`
  width: 60%;
  margin: 1rem auto;
  ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) {
    display: block;
  }
`;

 const NavBar = styled.div`
  position: absolute;
  top:0;
  left:0;
  width: 100%;
  height: 65px;
  background: white;
  box-shadow: 0 0.5px 10px rgba(0,0,0,0.3);
  ${props=>props.theme.utils.centerContent()}
`;


 const Title = styled.h2`
  margin: 2rem 0;
  font-weight: 600;
  text-align: center;
  font-size: 30px;
  color: #202124;
  opacity: 0.7;
  width: 100%;
`;

 const Subtitle = styled.h4`
font-weight: 400;
text-align: center;
color: #5F6469;
font-size: 16px;
margin: 0.5rem 0;  

`;

 const NavTextContainer = styled.div`
  width: 90%;
  text-align: left;

`;

 const IconContainer = styled.div`
  position: absolute;
  left: 1rem;
  top: 1rem;
`;

 const Icon = styled.i`
  vertical-align: bottom;
  cursor:pointer;
`;

 const Img = styled.img`
width: 150px;
margin: 1rem auto;
text-align: center;

`;


 const Container = styled.div`
  width:80%;
  margin: 2rem auto;
   padding-top: 65px;
   text-align: center;
`;



 

 const Button = styled.button`

  position: relative;
  padding: 0.6rem 1rem;
  
  
  margin: 2rem 0;
  border-radius: 3px;
  overflow: hidden;

  background: ${props=>props.disabled?"gray":"#3bc2b8"};
  color: white;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  border:0;
  ${props=>props.disabled && " cursor:not-allowed;"}

 
`;

 const Content = styled.div`
    width: 80%;
    margin: 0 auto;
   ${props=>props.theme.utils.rowContent()  }
    @media (max-width: 700px) {
    width: 90%;
  }
`;

 const Logo = styled.img`
  
  width: 40px;
  height: auto;
 
`;


const logo = require("static/logo.png")
const _ = require('lodash');



const AddNew = ()=>{


    const {
    auth,
    actions
    } = useContext(SteemContext);

   const [title, setTitle] = useState("");
   const [body, setText] = useState("");
   const [tags, setTags] = useState("");
    const [redirect, setRedirect] = useState(null);
    const [disabled, setDisabled] = useState(true);

       const [loading, setLoading] = useState(false);
    useEffect(()=>{
      setDisabled(!(title && body && tags))
      
    })
const createAd = ()=>{

  const tagsA = tags.split(/(?:,| )+/);


  if(!tagsA[0])
    {alert("You must need to add a tag")
return null}

  const category =  tagsA[0]


  const post_params = {
  title,
  body,
  category,
  permlink: uuid4(),
  jsonmetadata:{tags:tagsA}
}
setLoading(true)
 actions.post(post_params).then((r)=>{

  setLoading(false)
  setRedirect(`${auth.name}/${post_params.permlink}`)


 }).catch((e)=>{
  setLoading(false)
  alert("There is an error. Try againg later...")
  console.error(e)
 })


}
    if(redirect)
      return <Redirect to={"/post/"+redirect}/>

    if(loading)
      return <Loading/>
     return(
    
        <Card> 
<Link to="/">
        <RoundClose
                 
          style={{
            position: "absolute",
            color: "rgba(0,0,0,0.6)",
            cursor:"pointer",
            top:"0.5rem", 
            right:"0.5rem",
            fontSize:"30px",
          }}/>
</Link>
<Link to="/">
<Logo
    src={logo}/>
      </Link>
           
            <Content>
             
             <Title>
            Create new ad
            </Title>
            <Input value={title}
              placeholder= {"Title"}
              onChange={(e)=>{setTitle(e)}}/>
         
            <textarea 
             value={body} onChange={(e)=>{setText(e.target.value)}}
            style={{width:"100%", margin:"1rem 0"}}
            placeholder="Write something here..." rows="20">
              
            </textarea>
               
            <Input
              placeholder={"Tags"}
             value={tags} onChange={(e)=>{setTags(e)}}/>
            </Content>
               
             
             
             <Button disabled={disabled}

             onClick={async ()=>{

            
              createAd()
           }}>Post</Button>
          </Card>
   
      );


}


export default AddNew