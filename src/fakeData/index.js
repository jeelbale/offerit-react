import fakeData1 from  "fakeData/angielb"
import fakeData2 from  "fakeData/bernie"
import fakeData3 from  "fakeData/repollo"

const data = [fakeData1, fakeData2, fakeData3];

export default data;