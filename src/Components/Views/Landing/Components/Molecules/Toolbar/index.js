import React, { Component } from 'react';
import styled ,{keyframes, css, ThemeProvider}from 'styled-components';
import ReactDOM from 'react-dom';
import { Redirect, Link,BrowserRouter, Route, Switch, withRouter} from 'react-router-dom';

import HelpIcon from "react-md-icon/dist/RoundHelpOutline";

const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  margin: 0 0.5rem;
  border-radius: 30px;
  overflow: hidden;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  cursor: pointer;
  font-weight: normal;
  font-size: 14px;
  border: ${props => (!props.transparent ? "0" : `solid 1px ${props.theme.color.orange}`)};;
`;
		
const _ = require('lodash');
const Toolbar = withRouter((props)=> {
                     
const UserCirlce = styled.div`
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};
`;

 const Logo = styled.img`
    padding-top:3px;
    width: 55px;
    margin-left: -10px;
`;

 const Nav = styled.nav`
    top:0;
    left:0;
    position: fixed;
    overflow: hidden;
    width: 100%;
    background: white;
    color: ${props=>props.theme.color.navbarText};
    height: 60px;
    ${props=>props.theme.utils.rowContent()}
    z-index: 1;
    box-shadow: 0 0.5px 2px rgba(0,0,0,0.2);
   
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent()}
    width: 100%;
    margin: 0 5%;
  
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Item = styled.div`
    position: relative; 
    height: 60px;
    color:${props=> props.active?"black":"#5F6469" };
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 0.5rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}
    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `}
    a{
        text-decoration: none;
        color:${props=> props.active?"black":"#5F6469" };
        font-size: 18px;
    }
`;
 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;
const Icon = styled.i`
  vertical-align: bottom;
`;
const TabSet = styled.div`
 display: ${props=>props.notNavbar?"none": "contents"};
  @media (max-width: 700px) {
    display: none;
  }
`;

const TabItem = ({children, active, onClick})=>{
  const TabSelector = styled.div`
    background: ${(props)=>props.theme.color.orange};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 16px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
    margin:0 auto;
`;

  return (
    <Item 
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer
      >
       {children}
        {active &&  <TabSelector/>}
    </Item>
    )
}

const logo = require("static/imgs/logoCrop.jpg");

class Header extends Component
{
  constructor(props) 
  {
      super(props)
      this.state = {
        sidebar: false
      }
  }
  
  render() {
    const tab = this.props.location.pathname
    return (    
          <Nav >
          <NavContent>
          <AlignStart>
            
         


          <Item  
            
            style={{ 
              fontSize:"30px", 
              fontWeigth:"600",
              marginRight:"1rem"
            }}>
          <Link to="/#inicio">

          <Logo src={logo} />
           </Link>
          </Item>
          <Item >
            <Link to="/#inicio"
            style={{ 
              fontSize:"29px", 
              fontWeight:"500",
              "textDecoration":"none",
              color:"rgba(0,0,0,0.5)"
            }}>
            RAYA
            </Link>
          </Item>
           
            </AlignStart>

            <AlignEnd> 

              <Item>
                <Link 
                  style={{ 
                  textDecoration:"none",
                  color:"gray"}}
                  to="/login">
                    <Button transparent>
                    Iniciar sesión
                    </Button>
                </Link>   
              </Item>

           <Item hide>
          <Link 
          style={{ 
          textDecoration:"none",
          color:"gray"
          }}
          to="/registrar">
            <Button >
              Registrarme
            </Button>
          </Link>   
          </Item>

        

          </AlignEnd>
          </NavContent>
          </Nav>
      );
  }
}

return(<Header {...props}/>)              
}
)
export default Toolbar