import React, { Component } from 'react';
import styled , { 
  keyframes, 
  css, 
  ThemeProvider } from 'styled-components';

import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';

const _ = require('lodash');
//Code
const CARD_HEIGHT= "430px"
//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 100%;
    z-index: 0;
    position: relative;
    margin: 0rem auto;
    padding-bottom: 50px;
    text-align: left;

     @media (max-width: 700px) {
      margin-top:3px;
    }
 
    
  }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;


const Title = styled.h1`
  margin: 0;
  font-weight: 400;
  font-size: 40px;
  width: 40%;
 margin: 0 auto;
  color:#202124;
  text-align: right;
  line-height: 1;
  @media (max-width: 700px) {
     width: 90%;
     margin: 3rem auto;
    font-size: 20px;
    text-align: center;
  }

`;





 const Row = styled.div`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}
    @media (max-width: 700px) {
    display: block;
  }
`;



 const ImgContainer = styled.div`
    width: 40%;
    
    margin: 0 auto;
    overflow: hidden;
    text-align:left;
    img{
    
     margin: 0 auto;
    }

        @media (max-width: 700px) {
    display: none;
  }
`;

 const Img = styled.img`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}
    @media (max-width: 700px) {
    display: block;
  }

`;

const cuadros = require("static/imgs/download.png");
const cover = require("static/imgs/INTROPIC.jpg");



class App extends Component
{
  constructor(){
    super();
    this.state = {
      register: false
    }
  }
  


  componentWillMount() {
    
  }
  render() {

    return (
      <React.Fragment>

      <Container>
   
      <br/>
      <br/>
            <Row>
   <ImgContainer>
        <Img src={cuadros} />  
        </ImgContainer>
        <Title>Informes: (33) 3671-0942</Title>
    

        </Row>

        <img src={cover}  style={{width:"100%"}}/>  
        <br/>
         <br/>
          <br/>
 
        </Container>
      </React.Fragment>
    );
  }
}

export default App
//rotate(90)