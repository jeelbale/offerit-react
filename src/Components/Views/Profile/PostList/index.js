import React, { useContext, useState, useEffect} from 'react'; 
import styled from 'styled-components';
import PostCard from "Components/Molecules/PostCard"

import SteemAsync from 'react-steem-provider/SteemAsync';
//import fakeData from  "fakeData/angielb"


const _ = require('lodash');


//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 50%;
    background: #fefefe;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
    left: 20%;
    margin: 0rem auto;

    min-height: 800px;
    padding: 2rem 5%;
 
 
    padding-bottom: 50px;
    text-align: left;
  
    @media (max-width: 700px)
    {
         width: 100%;
         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 500;
  font-size: 18px;
  width: 90%;
  margin: 1rem auto;
  color:#202124;
  text-align: left;
  line-height: 1;

`;

const Subtitle = styled.h2`
  width: 90%;
  margin: 1rem auto;
  font-weight: 400;
  font-size: 14px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;

const ImgContainer = styled.div`
  width: 100%;
  height: 200px;
  margin: 0 auto;
  text-align:center;
  overflow: hidden;
  position: relative;
`;

const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;

const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  position: absolute;
  bottom:1rem;
  right: 1rem;
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  border:${props => (!props.transparent ? "none" : "1px solid "+ props.theme.color.orange)};
`;

const Card = styled.div`
  width: 90%;
  margin: 1.2rem auto;
  height: 240px;
  background: white;
  border-radius: 2px;

  text-align: center;

  overflow: hidden;
  position: relative;

  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }

`;

 const Row = styled.div`
  width: 100%;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;



const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 18px;
  opacity: 0.9;
  text-align: left;
`;


const Products = ({ name})=>{

  const [loading, setLoading] = useState(true);
  const [post, setPost] = useState([]);

  useEffect(()=>{

   SteemAsync.getDiscussionsByBlog(name, 10).then(
    (posts)=>{ 
    
      setPost(posts)
      setLoading(false); 

    }).catch((err)=>console.error(err));


  },[])





  return (
      <React.Fragment>

        <Container>
          <H1>{name} | ads</H1>
          <br/>
          <hr style={{opacity:"0.4"}}/>
          {loading && "loading..."}
          <Row>

          {_.map( _.filter(post, (p)=>p.author === name),(post, index)=> (<PostCard key={post.id} id={index} post={post}/>)) }

          </Row>
        </Container>      
      </React.Fragment>
    );
}

export default Products;


