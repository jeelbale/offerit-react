import React, { useContext } from 'react';
import {BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Loading from "Components/Molecules/Loading";

import Dashboard from "Components/Views/Dashboard";
import Post from "Components/Views/Post";
import AddNew from "Components/Views/AddNew";
import Profile from "Components/Views/Profile";


import Login from "Components/Views/Login";
import Logout from "Components/Molecules/Logout"
import scrollToTopOnMount from "Components/Utils/scrollToTopOnMount"


import {AuthenticatedRouteRedirect, AuthenticatedRoute} from "Routes/Utils"

 import {SteemContext} from 'react-steem-provider';


/*
Routes   
 dashboard

My stuff
*/

/*
const ModalsManager = (props)=>{
  const {openOrder, changeOpenOrder} = useContext(OrdersContexts);




  return( 
    <React.Fragment>
      {openOrder  &&   <OrderModal close={()=>{ changeOpenOrder(null) }} />}
    </React.Fragment>
    )
}

*/
const App =  (props)=>
{

    const {
    loading,
    auth
   
    } = useContext(SteemContext);
    
  if(loading)
    return <Loading />
  return (
 
        <BrowserRouter>
        <React.Fragment>

         <Switch>
            <AuthenticatedRouteRedirect 
              exact
              path="/dashboard/:category?" 
              authenticated={true}
              component={scrollToTopOnMount(Dashboard)}
              />


            <AuthenticatedRouteRedirect 
              exact
              path="/post/:author/:permlink" 
              authenticated={true}
              component={scrollToTopOnMount(Post)}
              />

            <AuthenticatedRouteRedirect 
              
              path="/profile/:name" 
              authenticated={true}
              component={scrollToTopOnMount(Profile)}
              />

            <AuthenticatedRouteRedirect 
              exact
              path="/add-new" 
              authenticated={auth}
              component={scrollToTopOnMount(AddNew)}
              />  

            <Route
              path="/"
              authenticated={true}
              component={(props) => { 
              return (<Redirect  to="/dashboard" /> )  
              }}   
            />
          </Switch>

          <Route 
          exact  
          path="/login" 
          component = {Login}/> 

          <Route  
          path="/logout"
          component={Logout} />
          
          </React.Fragment>
        </BrowserRouter>
  );
}

export default App;