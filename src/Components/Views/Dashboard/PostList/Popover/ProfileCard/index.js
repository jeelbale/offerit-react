import React, {Component, Fragment} from 'react';
import { Container, PseudoContainer, Content, Description, ImageContainer, Image, Information, Name, Email, AccountIcon, ExploreIcon, SettingsIcon, ExitIcon} from './elements.js';
import BaselineAccountCircle from "react-md-icon/dist/BaselineAccountCircle";
import BaselineHistory from "react-md-icon/dist/BaselineHistory";
import ListItem from './ListOptions/ListItem/';
import LineBreak from './ListOptions/LineBreak/';
import styled from 'styled-components';
const Text = styled.div `
	font-family: Rubik, 'sans-serif';
	min-height: 40px;
	width: 100%;
	cursor: pointer;
	padding: 0.1rem 0;
	padding-left: 24px;
	background: white;
	display: flex;
	align-items: center;
	font-size: 18px;

	&:hover {
		background: #F0F0F0;
	}

	a {
		text-decoration: none;
		color: #727272;
		display: flex;
		align-items: center;
	}

	a:hover {
		color: ${props => props.theme.color.blackCode};
	}
`


const  Menu  = ({setFilter})=>
	
			(<Fragment>
							
							<Content>

							
								<Text onClick={()=>setFilter("Trending")}>
								Trending
								</Text>
										
						
			
								
						<LineBreak/>

							
								<Text onClick={()=>setFilter("New")}>
								New 
								</Text>
										
						
			
								
						<LineBreak/>		
			
			
			
								<Text onClick={()=>setFilter("Hot")}>
								Hot
								</Text>
										
						
			
								
						<LineBreak/>	
			
								<Text onClick={()=>setFilter("Promoted")}>
								Promoted
								</Text>
								

						


							</Content>
						</Fragment>
					)
	

export default Menu;
