export default {
	centerContent:(type)=>` 
			display: flex;
			flex-direction: column;
			justify-content: center;

			text-align:center;
			align-items: center;
  	`,

  	fullscreen:(type)=>` 
			position: fixed;
			top:0rem;
			left:0rem;
			right:0rem;
			bottom:0rem;
			width:100%;
			height:100vh;
			z-index: 999999999999999999999999999999999999999999999;
			background: rgba(33,33,33,0.8);	
  	`,
  	 rowContent:(type)=>` 
		display: flex;
	    justify-content: space-between;
	    flex-direction: row;
	    align-content: flex-end;
	    flex-flow: row wrap;
  	`
}
