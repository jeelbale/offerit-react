

/**
 * The css property used for elevation. In most cases this should not be changed. It is exposed
 * as a variable for abstraction / easy use when needing to reference the property directly, for
 * example in a `will-change` rule.
 */
//const umbra_color =  "rgba(0,0,0, 0.2)";
//const penumbra_color =  "rgba(0,0,0, 0.14)";
const ambient_color =  "rgba(0,0,0, 0.50)";
//const elevation_property =  "box-shadow";
/*
 * The default duration value for elevation transitions.
 */
//const elevation_transition_duration =  "280ms";
/*
 * The default easing value for elevation transitions.
*/
//const elevation_transition_timing_function = animation_standard_curve_timing_function;
/*
const elevation_umbra_map = [
	"0px 0px 0px 0px",
	"0px 2px 1px -1px",
	"0px 3px 1px -2px",
	"0px 3px 3px -2px",
	"0px 2px 4px -1px",
	"0px 3px 5px -1px",
	"0px 3px 5px -1px",
	"0px 4px 5px -2px",
	"0px 5px 5px -3px",
	"0px 5px 6px -3px",
	"0px 6px 6px -3px",
	"0px 6px 7px -4px",
	"0px 7px 8px -4px",
	"0px 7px 8px -4px",
	"0px 7px 9px -4px",
	"0px 8px 9px -5px",
	"0px 8px 10px -5px",
	"0px 8px 11px -5px",
	"0px 9px 11px -5px",
	"0px 9px 12px -6px",
	"0px 10px 13px -6px",
	"0px 10px 13px -6px",
	"0px 10px 14px -6px",
	"0px 11px 14px -7px",
	"0px 11px 15px -7px"
];
const elevation_penumbra_map = [
  "0px 0px 0px 0px",
  "0px 1px 1px 0px",
  "0px 2px 2px 0px",
  "0px 3px 4px 0px",
  "0px 4px 5px 0px",
  "0px 5px 8px 0px",
  "0px 6px 10px 0px",
  "0px 7px 10px 1px",
  "0px 8px 10px 1px",
  "0px 9px 12px 1px",
  "0px 10px 14px 1px",
  "0px 11px 15px 1px",
  "0px 12px 17px 2px",
  "0px 13px 19px 2px",
  "0px 14px 21px 2px",
  "0px 15px 22px 2px",
  "0px 16px 24px 2px",
  "0px 17px 26px 2px",
  "0px 18px 28px 2px",
  "0px 19px 29px 2px",
  "0px 20px 31px 3px",
  "0px 21px 33px 3px",
  "0px 22px 35px 3px",
  "0px 23px 36px 3px",
  "0px 24px 38px 3px"
];*/
const elevation_ambient_map = [
  "0px 0px 0px 0px",
  "0px 1px 3px 0px",
  "0px 1px 5px 0px",
  "0px 1px 8px 0px",
  "0px 1px 10px 0px",
  "0px 1px 14px 0px",
  "0px 1px 18px 0px",
  "0px 2px 16px 1px",
  "0px 3px 14px 2px",
  "0px 3px 16px 2px",
  "0px 4px 18px 3px",
  "0px 4px 20px 3px",
  "0px 5px 22px 4px",
  "0px 5px 24px 4px",
  "0px 5px 26px 4px",
  "0px 6px 28px 5px",
  "0px 6px 30px 5px",
  "0px 6px 32px 5px",
  "0px 7px 34px 6px",
  "0px 7px 36px 6px",
  "0px 8px 38px 7px",
  "0px 8px 40px 7px",
  "0px 8px 42px 7px",
  "0px 9px 44px 8px",
  "0px 9px 46px 8px"
];

export function elevation(n = 1, color = ambient_color) {
  return `
    box-shadow: ${elevation_ambient_map[(n >= 0 && n <= 24)? n:0]} ${color};
  `;
}


/*
@function mdc-elevation-transition-rule(
  $duration: $mdc-elevation-transition-duration,
  $easing: $mdc-elevation-transition-timing-function) {
  @return #{$mdc-elevation-property} #{$duration} #{$easing};
}

/**
 * Applies the correct css rules needed to have an element transition between elevations.
 * This mixin should be applied to elements whose elevation values will change depending on their
 * context (e.g. when active or disabled).
 */

/*
@mixin mdc-elevation-transition(
  $duration: $mdc-elevation-transition-duration,
  $easing: $mdc-elevation-transition-timing-function) {

  transition: mdc-elevation-transition-rule($duration, $easing);
  will-change: $mdc-elevation-property;
}
.mdc-elevation-transition {
  @include mdc-elevation-transition;
}
*/