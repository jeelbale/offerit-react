import React, { useContext, useState } from 'react'; 
import styled from 'styled-components';

import BaselineKeyboardArrowUp from "react-md-icon/dist/BaselineKeyboardArrowUp";
import BaselineKeyboardArrowDown from "react-md-icon/dist/BaselineKeyboardArrowDown";
import BaselineComment from "react-md-icon/dist/BaselineComment";
import fakeData from  "fakeData/angielb"
import {Link} from "react-router-dom"

 






const _ = require('lodash');

//Code
const CARD_HEIGHT= "430px"


//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 60%;
    background: #fefefe;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
    left: 20%;
    margin: 0rem auto;
  
    min-height: 800px;
    padding: 2rem;
 
 
    padding-bottom: 50px;
    text-align: left;
  
    @media (max-width: 700px)
    {
         width: 100%;
         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 500;
  font-size: 18px;
  width: 90%;
  margin: 1rem auto;
  color:black;
  text-align: left;
  line-height: 1;

`;

const Subtitle = styled.h2`

  margin: 1rem auto;
  font-weight: 400;
  font-size: 14px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;




const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  position: absolute;
  bottom:1rem;
  right: 1rem;
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  border:${props => (!props.transparent ? "none" : "1px solid "+ props.theme.color.orange)};
`;

const Card = styled.div`
  width: 100%;
  margin: 1.2rem auto;
  
  background: white;
  border-radius: 2px;
  border: 1px solid rgba(0,0,0,0.1);
  text-align: center;

  overflow: hidden;
  position: relative;

  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }

`;

 const Row = styled.div`
  width: 90%;
  margin: 0 auto;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;


const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 22px;
  opacity: 0.9;
  text-align: left;
`;


const ImgContainer = styled.div`
width: 100%;
height: 250px;
overflow: hidden;

`;


const Img = styled.img`
  width: 100%;
 
`;


const Item = styled.div`
  background: red;
  margin: 0 0.5rem;
  height: 60px;
  ${props=>props.theme.utils.centerContent()}
 
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;

    min-width: 0;
    height: 100%;
    flex: 1;
   

`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;


const Divider = styled.div`  
  width:1px;
  background: rgba(0,0,0,0.1);
  height: 30%;

`;
const Circle = styled.div`  
  border:1px solid rgba(0,0,2,0.2);
  width: 20px;
  height:20px;
  border-radius: 50%;
   ${props=>props.theme.utils.centerContent()}
  

`;


const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;


const ImgCircleContainer = styled.div`  
  border:1px solid rgba(0,0,2,0.2);
  width: 30px;
  height:30px;
  border-radius: 50%;
overflow: hidden;
background: purple;
opacity: 0.6;
  
`;



 const Header = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 17px;
  width: 90%;
  margin: 1rem auto;
  color: rgba(0,0,0,0.7)  ;
  text-align: left;
  line-height: 1;

`;

const SubHeader = styled.h2`
  width: 90%;
  height: 3.4rem;
  overflow: hidden;
  margin: 1rem auto;
  font-weight: 400;
  font-size: 16px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;



const ItemPostList = ({ product})=>{



  return (
      <React.Fragment>

            <Card to="">

    <Row>
              <AlignStart>

            <Item><ImgCircleContainer>   </ImgCircleContainer>
</Item>


              <Item> 
               <Link to={"/profile/"+fakeData.author} style={{textDecoration:"none"}}>      
              <Title>
              
            {fakeData.author}
              </Title>
              </Link>

              </Item>
              <Item>

              <Link to={"/dashboard/"+fakeData.category} style={{textDecoration:"none"}}>

              <Subtitle>in {fakeData.category}</Subtitle>
              <Link>


              </Item>
             

  
</AlignStart>

              </Row>
            

              <ImgContainer>
               <Img src={fakeData.json_metadata.image[0]} />
              </ImgContainer>

              <Header>
              {fakeData.title}
              </Header>

              <SubHeader>
             { fakeData.body.replace(/<[^>]*>/g, "").replace(/(?:https?|ftp):\/\/[\n\S]+/g, '').replace(/\r?\n|\r/g," ")
}
              </SubHeader>
              <Row>
              <AlignStart>

              

 <Item><Circle><BaselineKeyboardArrowUp/></Circle></Item>
              <Item><Circle><BaselineKeyboardArrowDown/></Circle></Item>
              <Item>${fakeData.author_rewards}</Item>
               <Item >  <Divider /> </Item>
               <Item><BaselineKeyboardArrowUp/></Item>
              <Item > {fakeData.active_votes.length} </Item>
              <Item >  <Divider /> </Item>

              <Item><BaselineComment/></Item> 
              <Item> {fakeData.replies.length} </Item>
            

</AlignStart>

              </Row>

            </Card>

     
      </React.Fragment>
    );
}

export default ItemPostList;


/*

(!this.props.orders.value.products[product.id] && !this.props.disbled) &&
  <Button  
    onClick={()=>{
      this.props.orders.addProductToCar(product);
    }}>+</Button>
*/