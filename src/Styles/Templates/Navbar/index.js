import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import styled , { keyframes } from 'styled-components' ;

export const Nav = styled.nav`
  top:0;
  position: fixed;
  overflow: hidden;
  background: #212121;

  width: 100%;
  height: ${props=>props.theme.const.navbarHeight}px;
  ${props=>props.theme.utils.rowContent()}
  ${props => props.view && props.theme.elevation(1)}
`;

export const NavContent = styled.nav`
  ${props=>props.theme.utils.rowContent()}
  width: 100%;
  margin: 0 20px;
`;

export const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;    
`;

export const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

export const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

export const Item = styled.div`
  position: relative; 
  height: ${props=>props.theme.const.navbarHeight}px;
  ${props=>props.theme.utils.centerContent()}
  ${props=>props.cursor && "cursor: pointer;"}
  ${props=>props.margin && "margin: 0 0.5rem;"}

  min-width: 45px; 
`;

export const TabItem = ({children, active, onClick})=>{
    
  const TabSelector = styled.div`

  background: ${(props)=>props.theme.secondary};

  position: absolute;
  width: 100%;
  height: 3px;
  font-size: 16px;
  border-radius: 5px 5px 0 0;
  bottom: 0;
  left:0;
  right:0;
`;

  return (
    <Item 
      onClick={onClick} 
      active={active}
      cursor
      >
       {children}
        {active &&  <TabSelector/>}
    </Item>
    )
}

//=============================================================================
export const UserCirlce = styled.div`

  height: 34px;
  width: 34px;

  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};

`;

export const Logo = styled.img`
  border-radius:50%;
  background: transparent;
`;