import React, {Component, Fragment, useContext} from 'react';
import { Container, PseudoContainer, Content, Description, ImageContainer, Image, Information, Name, Email, AccountIcon, ExploreIcon, SettingsIcon, ExitIcon} from './elements.js';
import BaselineAccountCircle from "react-md-icon/dist/BaselineAccountCircle";
import BaselineHistory from "react-md-icon/dist/BaselineHistory";
import ListItem from './ListOptions/ListItem/';
import LineBreak from './ListOptions/LineBreak/';
import styled from 'styled-components';
 import {SteemContext} from 'react-steem-provider';
const Text = styled.div `
	font-family: Rubik, 'sans-serif';
	min-height: 40px;
	width: 100%;
	cursor: pointer;
	padding: 0.1rem 0;
	padding-left: 24px;
	background: white;
	display: flex;
	align-items: center;
	font-size: 18px;

	&:hover {
		background: #F0F0F0;
	}

	a {
		text-decoration: none;
		color: #727272;
		display: flex;
		align-items: center;
	}

	a:hover {
		color: ${props => props.theme.color.blackCode};
	}
`

 
const  Menu  = ()=>
	
			{
				const {
    auth,
    logout,
    steemConnect
   
    } = useContext(SteemContext);



				return (<Fragment>
										
										<Content>
			
												<ListItem
											style= {{padding:"0"}}
											 link={`/profile/${auth.name}`}
											icon={null} description="Profile"/>
													
									
						
											
									<LineBreak/>
			
										<ListItem
											style= {{padding:"0"}}
												 link={`/profile/${auth.name}/wallet`}
											icon={null} description="Wallet"/>
													
									
						
											
									<LineBreak/>		
						
						
						
											<ListItem
											style= {{padding:"0"}}
											 link="/" 
											icon={null} description="Go to ads"/>
													
									
						
											
									<LineBreak/>	
						
											<Text onClick={logout}>
											Logout
											</Text>
											
			
									
			
			
										</Content>
									</Fragment>
								)
				}

export default Menu;
