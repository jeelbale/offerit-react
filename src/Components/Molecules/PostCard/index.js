import React, { useState, useEffect, useContext } from 'react';
import styled, {keyframes} from 'styled-components';

import BaselineKeyboardArrowUp from "react-md-icon/dist/BaselineKeyboardArrowUp";
import BaselineKeyboardArrowDown from "react-md-icon/dist/BaselineKeyboardArrowDown";
import BaselineComment from "react-md-icon/dist/BaselineComment";

import {Link} from "react-router-dom"
import {timeAgo} from 'JS/date';
import {parserSteemRep, parserSteemSimpleRep} from 'react-steem-provider/Helpers';

import SteemAsync from 'react-steem-provider/SteemAsync';
import {SteemContext} from 'react-steem-provider';

const _ = require('lodash');
 
//Code
const CARD_HEIGHT= "430px"


const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;

 const Container = styled.div`
    width: 60%;
    background: #fefefe;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
    left: 20%;
    margin: 0rem auto;
  
    min-height: 800px;
    padding: 2rem;
 
 
    padding-bottom: 50px;
    text-align: left;
  
    @media (max-width: 700px)
    {
         width: 100%;
         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;
 const Title = styled.h1`
  margin: 0;
  font-weight: 500;
  font-size: 18px;
  width: 90%;
  margin: 1rem auto;
  color:black;
  text-align: left;
  line-height: 1;

    @media (max-width: 700px)
    {
       font-size: 14px;
         
    }
`;

const Subtitle = styled.h2`
  margin: 1rem auto;
  font-weight: 400;
  font-size: 14px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;

    @media (max-width: 700px)
    {
      font-size: 11px;
         
    }
`;




const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  position: absolute;
  bottom:1rem;
  right: 1rem;
  box-shadow:  0 0.3px 5px rgba(0,0,0,0.2);
  overflow: hidden;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  border:${props => (!props.transparent ? "none" : "1px solid "+ props.theme.color.orange)};
`;

const Card = styled.div`
  width: 100%;
  margin: 1.2rem auto;
  
  background: white;
  border-radius: 2px;
  border: 1px solid rgba(0,0,0,0.1);
  text-align: center;
  overflow: hidden;
  position: relative;
  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }
`;



 const Row = styled.div`
  width: 90%;
  margin: 0 auto;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;




const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 22px;
  opacity: 0.9;
  text-align: left;
`;
const ImgContainer = styled.div`
width: 100%;
height: 250px;
overflow: hidden;


 @media (max-width: 700px)
    {
         height: auto;

         
    }

`;


const Img = styled.img`
  width: 100%;
 
`;


const Item = styled.div`

  margin-right: 0.5rem;
  height: 60px;
  ${props=>props.theme.utils.centerContent()}
   @media (max-width: 700px)
    {
       font-size: 14px;
         
    }
 
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;

    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;

`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;


const Divider = styled.div`  
  width:1px;
  background: rgba(0,0,0,0.1);
  height: 30%;

`;
const Circle = styled.div`  
  border:1px solid rgba(0,0,2,0.3);
  cursor: pointer;
  &:hover{
    background: rgba(0,0,0,0.4);
  }
  width: 20px;
  height:20px;
  border-radius: 50%;
   ${props=>props.theme.utils.centerContent()}

  ${props=>props.voted && ` 
     background: #b79ee6;
   color: white;`}
  


${props=>props.load && `background: green;`}

`;



const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;


const ImgCircleContainer = styled.div`  
  border:0;
  width: 36px;
  height:36px;
  border-radius: 50%;
  overflow: hidden;
  border: 1px solid rgba(0,0,0,0.2);

    @media (max-width: 700px)
    {
        width: 28px;
  height:28px;
         
    }

`;



 const Header = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 17px;
  width: 90%;
  margin: 1rem auto;
  color: rgba(0,0,0,0.7)  ;
  text-align: left;
  line-height: 1;

`;

const SubHeader = styled.h2`
  width: 90%;
  height: 3.4rem;
  overflow: hidden;
  margin: 1rem auto;
  font-weight: 400;
  font-size: 16px;
  color: #5F6469;
  opacity: 0.77;
  text-align: left;
`;

/*
client.vote(voter, author, permlink, weight, function (err, res) {
  console.log(err, res)
});

*/

const ItemPostList = ({ post, id})=>{

  const [loading, setloading] = useState(true);
  const [author, setAuthor] = useState("");
  const [voted, setVote] = useState(0);
  const [loadVote, setLoadVote] = useState(false);

  

    const {
      auth,
      actions,
      steemConnect
    } = useContext(SteemContext);


 


   

    const Vote = (w = 1000)=>{

      if(auth)
     { setLoadVote(true)
           steemConnect.vote(auth.name, post.author, post.permlink, w, (err, res)=> {
         if(err)
           alert("Error voting");
         else
           {
             setVote(voted + w);
     
           }
     
           setLoadVote(false)
     
     
         });}
    }

    useEffect(()=>{


if(auth)
    {const myvote = post.active_votes.filter((a)=>a.voter === auth.name)
    
        const votes = myvote.map((vote)=> parseFloat(vote.percent))
        const add =  votes.reduce((a, b) => a+b, 0);
    
    
    
         setVote(add);}

    },[])


   useEffect(()=>{
    SteemAsync.getUserAccount(post.author).then(
    (response)=>{
      try{
        const a = JSON.parse(response[0]["json_metadata"]).profile.profile_image; 
        setAuthor(a)       
      }
      catch(e){
        
      }
       //setloading(false)
     }).catch(
      (err)=>console.error(err)) 

    },[])


   useEffect(()=>{

       SteemAsync.getUserAccount(post.author).then(
      (response)=>{


      try{
        const a = JSON.parse(response[0]["json_metadata"]).profile.profile_image; 
        setAuthor(a)      
       
      }
      catch(e){
        
      }

       setloading(false)

     }).catch(
      (err)=>console.error(err)) 

    },[])



  


  if(!post)
    return null;

  try
  { 
    post["json_metadata"]= JSON.parse(post.json_metadata)}
  catch(e){
   // console.log("Detect what is happening here",e, post);
  }


  return (
      <React.Fragment>

            <Card to={`/post/${post.author}/${post.permlink}`}>

              <Row>
              <AlignStart>

              <Item style={{width:"38px"}}>
             {author && <ImgCircleContainer><Img src={author} /></ImgCircleContainer>}
              </Item>
              <Item> 
              <Link to={"/profile/"+post.author} style={{textDecoration:"none"}}>   
                <Title>
                {post.author}
                </Title>
              </Link>
              </Item>
              <Item> 
              <Link to={"/profile/"+post.author} style={{textDecoration:"none", color:"gray"}}>   
                ({parserSteemRep(post.author_reputation)})
              </Link>
              </Item>

              <Link to={"/dashboard/"+post.category} style={{textDecoration:"none"}}>
              <Item><Subtitle>in {post.category}</Subtitle></Item>
              </Link>
              <Item>
                  <Subtitle> -  {timeAgo(post.created)}</Subtitle>
              
              </Item>

              </AlignStart>

              </Row>
            
<Link to={`/post/${post.author}/${post.permlink}`} style={{textDecoration:"none"}}>

             {post.json_metadata.image && <ImgContainer>
               <Img src={post.json_metadata.image[0]} />
              </ImgContainer>}

              <Header>
              {post.title}
              </Header>

              <SubHeader>
                {post.body.replace(/<[^>]*>/g, "").replace(/(?:https?|ftp):\/\/[\n\S]+/g, '').replace(/\r?\n|\r/g," ")}
              </SubHeader>


              </Link>

              <Row>
              <AlignStart>
              {loadVote || !auth?<Item>{!auth?"":"Loading..."}</Item>:<React.Fragment> <Item>
                           <Circle  
                             load={loadVote}
                           voted={voted > 0} onClick={()=>{
                             if(voted <= 0)
                             Vote()}}>
                           <BaselineKeyboardArrowUp/></Circle></Item>
                           <Item><Circle 
                             load={loadVote}
                           voted={voted < 0}
                            onClick={()=>{
                             if(voted >= 0)
                             Vote(-1000)}}>
             
                           <BaselineKeyboardArrowDown/></Circle></Item></React.Fragment>}

              <Item>${post.pending_payout_value}</Item>
              <Item >  <Divider /> </Item>
              <Item><BaselineKeyboardArrowUp/></Item>
              <Item > {post.active_votes.length} </Item>
              <Item >  <Divider /> </Item>
              <Item> <Link to={`/post/${post.author}/${post.permlink}#comments`}><BaselineComment/></Link></Item> 
              <Item> {post.children} </Item>
              </AlignStart>
              </Row>

            </Card>

     
      </React.Fragment>
    );
}

export default ItemPostList;


/*

(!this.props.orders.value.products[product.id] && !this.props.disbled) &&
  <Button  
    onClick={()=>{
      this.props.orders.addProductToCar(product);
    }}>+</Button>
*/