import fakeData1 from  "./angieprofile"
import fakeData2 from  "./bernieprofile"
import fakeData3 from  "./blocktradesprofile"
import fakeData4 from  "./fintechprofile"
import fakeData5 from  "./demo"
import fakeData6 from  "./repolloprofile"

const data = {

	"angielb": fakeData1,
	"berniesanders": fakeData2,
	"blocktrades": fakeData3,
	"fintechresearch": fakeData4,
	"demo": fakeData5,
	"repollo": fakeData6
};

export default data;