import React, { Component } from 'react';
import styled  from 'styled-components';
import {Link} from 'react-router-dom';


const Container = styled.div`
  width: 100%;
  height: 350px;
  overflow: hidden;
  position: relative;
  margin: 0 auto;
  text-align: left;
  background: url("https://images.unsplash.com/photo-1463490093487-b0ffe9b8e140?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80");
  background-repeat: no-repeat;
  background-size: cover;
  opacity: 1;
  @media (max-width: 700px) {
  margin-top: -10px;
  height: 100vh;
  }
  ${props=>props.theme.utils.rowContent()}
`;


const Dark = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100vh;
    background: black;
    opacity: 0.4;
   
   `;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
    @media (max-width: 700px) {
      width: 100%;
      padding: 2rem 0;
    }
`;

 const Title = styled.h1`
    position: absolute;
    left:20%;
    top: 80px;
    font-weight: 400;
    font-size: 70px;
    width: 60%;
    margin: 0 auto;
    color: white;
    text-align: center;
    @media (max-width: 700px) {
      font-size: 45px;
      width: 90%;
      top: 200px;
      left: 5%;
    }

`;

 const Subtitle = styled.h2`
    width: 100%;
    font-weight: 500;
    font-size: 22px;
    color: #5F6469;
    text-align: left;
`;

 const ImgContainer = styled.div`
    width: 40%;
    margin: 0 auto;
    text-align:center;
    img{
      width:100%;
      margin: 0 auto;
    }
    @media (max-width: 700px) {
      display: none;
    }
`;

 const ImgCircle = styled.div`
  display: none;
  width: 250px;
  height: 250px;
  border-radius: 50%;
  overflow: hidden;
  img{
    width: 145%;
  }
  @media (max-width: 700px) 
  {
   display: inline-block;
  }
`;
 const Button = styled.button`
  position: relative;
  padding: 0.6rem 1rem;
  margin: 1rem 0.5rem;
  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? props.theme.color.orange : "transparent")};
  color: ${props => (!props.transparent ? "white" : props.theme.color.orange)};
  cursor: pointer;
  font-weight: normal;
    font-size: 18px;
  border: ${props => (!props.transparent ? "0" : `solid 1px ${props.theme.color.orange}`)};;
`;
 const Label = styled.label`
  font-size: 16px;
  text-align: center;
  font-weight: 500;
  a{
      color: #444444;
  }
  margin: 0 auto;
  margin-top: 2rem;
`;

 

 const Row = styled.div`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}

   @media (max-width: 700px) {
    display: block;
  }
`;


const cuadros = require("static/imgs/download.png");

class App extends Component
{
  constructor(){
    super();
    this.state = {
      register: false
    }
  }
  
  authWithGoogle() {
    
  }

  componentWillMount() {
    
  }
  render() {

    return (
      <React.Fragment>

   
        <Container >
      
        <Dark />

             <Title>
        El Mercado de Absastos a tu alcance.
        </Title>
        </Container>
    
         
       
      </React.Fragment>
    );
  }
}



export default App;