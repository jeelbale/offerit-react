import React, { Component } from 'react';
import styled ,{keyframes, css, ThemeProvider}from 'styled-components';
import ReactDOM from 'react-dom';
import {  Redirect, Link, 
          BrowserRouter, Route, 
          Switch, withRouter} from 'react-router-dom';

import firebase from "firebase"
import Orders from "Components/Views/SolicitudesList";
import Archivos from "Components/Views/Archivos";
import Usuario from "Components/Views/Usuario";
import Inicio from "Components/Views/Inicio";
import Sidebar from "Components/Molecules/Sidebar"
import Toolbar from "Components/Molecules/Toolbar"

const Body = styled.div`
      width: 100%;
      position: relative;
      top: 65px; 
      background: white;
      text-aling: left; 
      z-index:1;
`;
    
const Container = styled.div`
  width: 100%;
  margin-top: 20px;
`;
          
class Modal extends Component 
{
  constructor(props) 
  {
    super(props);
    this.state={
    }
  }

   createComponent(form)
  {
        //console.log(form)
  }


  render()
  {

    const close = this.props.close//?this.props.close:this.props.hideOnClick;
    
     return(
      <React.Fragment>

    <Body>
         
      <Container>
          <Switch>
            <Route  
                path={`${this.props.match.url}/configuracion`} 
                render={(props)=>(<h1>Configuracion{props.match.url}</h1>)}/>
            
          
                <Route 
                    exact 
                    path={`${this.props.match.url}/ordenes`} 
                    component={Orders}/>
                    
            
                <Route  
                    path={`${this.props.match.url}/ayuda`} 
                    render={(props)=>(<h1>Sitio de ayuda en constucción.</h1>)}/>
                
                 <Route  
                    path={`${this.props.match.url}/usuario`} 
                    component={Usuario}/>
                        
        
                <Route 
                    path={`${this.props.match.url}/`} 
                    component={Inicio}/>
                  
                </Switch>
        </Container>
        
       
        <Toolbar  auth={true}/>   
  </Body>

        
  </React.Fragment>
  );
  }
}

export default withRouter(Modal)