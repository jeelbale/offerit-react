import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Redirect, Route, Switch } from 'react-router-dom';

import {AuthenticatedRouteRedirect, AuthenticatedRoute} from "./Utils"
import Dashboard from "./Dashboard"
import Login from "Components/Views/Login"
import Logout from "Components/Molecules/Logout"
import Loading from "Components/Molecules/Loading"
import Toolbar from "Components/Molecules/Toolbar"
import Copyright from "Components/Views/Copyright"
import Register from "Components/Views/Register"

import {withFirebase} from "Files/Providers/Firebase"


const Container = styled.div`
	width: 80%;  
	position: fixed;
	top: 67px;
	left:20%;
	bottom:0;
	right: 0rem;
	overflow: hidden;
	overflow-y: scroll;
	background: white;
	@media (max-width: 700px)
	{
		left:0;
		width: 100%;    
	}
`;
                 
class Routes extends React.Component
{
  render()
  {
      
    if(this.props.loading)
      return <Loading />
      
    return (
    	<React.Fragment>
      	<Switch>
            <AuthenticatedRouteRedirect 
                path="/dashboard" 
                authenticated={this.props.user}
                component={Dashboard}
            />
            <Route  
              path="/copyright"
              component={Copyright} 
            />
            
            <Route  
              path="/privacidad"
              component={Copyright} 
            />
            
            <Route  
              path="/terminos"
              component={Copyright} 
            />

            <AuthenticatedRoute
              path="/"
              authenticated={this.props.user}
              component={(props) => { 
                return (<Redirect  to="/dashboard" /> )  
                }}   
            />

        </Switch>

      	<Route  
          path="/login"
          component={Login} />
          <Route  
          path="/registrar"
          component={Register} />
          
        <Route  
          path="/logout"
          component={Logout} />
              	
    	</React.Fragment>
    )
  }
}

export default withFirebase(Routes)