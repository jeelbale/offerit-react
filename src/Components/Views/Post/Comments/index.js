import React, { useContext, useState, useEffect } from 'react'; 
import styled from 'styled-components';

 import {SteemContext} from 'react-steem-provider';
import SteemAsync from 'react-steem-provider/SteemAsync';
import {parseSteemMarkdown} from 'react-steem-provider/Helpers';

import {Link} from "react-router-dom"
const _ = require('lodash');

const uuid4 = require('uuid/v4');

//Code
const CARD_HEIGHT= "430px"



const Markdown = styled.div`
  img{
    max-width: 100%;
  }


`;

//Icons
 const IconRoundClose = styled.div`
    color: red;
    position: absolute;
    top: 1rem; 
    right:1rem;
    font-size: 1.5rem;
    color: #000000;
    opacity: 0.54;
`;


 const Container = styled.div`
    width: 70%;
    background: white;
    border-radius: 5px;
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
    left: 20%;
    
    overflow: hidden;

    min-height: 800px;
    padding: 2rem 5%;
 
 
    padding-bottom: 50px;
    text-align: left;
    margin: 0 auto;
    @media (max-width: 700px)
    {
         width: 100%;
         
    }
`;

const TextContainer = styled.div`
    width: 60%;
    position: relative;
    margin: 0 auto;
    text-align: left;
    padding-top: 5rem;
`;

 const Title = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 18px;
  width: 90%;
  margin: 1rem auto;
  color:#202124;
  text-align: left;
  line-height: 1;

`;

const Subtitle = styled.h2`
  width: 90%;
  margin: 1rem auto;
  font-weight: 400;
  font-size: 16px;
  color: #5F6469;
  opacity: 1;
  text-align: left;
`;

const ImgContainer = styled.div`
  width: 100%;
  height: 200px;
  margin: 0 auto;
  text-align:center;
  overflow: hidden;
  position: relative;
`;

const ImgCircle = styled.img`  
  width: 100%;
  overflow: hidden;
`;

 const Button = styled.button`

  position: relative;
  padding: 0.6rem 1rem;
  
  
  margin: 2rem 0;
  border-radius: 3px;
  overflow: hidden;

  background: gray;
  color: white;
  cursor: pointer;
  font-weight: normal;
  font-size: 16px;
  border:0;

 
`;


const Card = styled.div`
  width: 90%;
  margin: 1.2rem auto;
  min-height: 140px;
  background: #f7f7f7;
  border-radius: 2px;

  text-align: center;

  overflow: hidden;
  position: relative;
   box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);

  &:hover{
    box-shadow:  0 0.2px 3px rgba(0,0,0,0.1);
  }

`;

 const Row = styled.div`
  width: 100%;
 ${props=>props.theme.utils.rowContent()}
  @media (max-width: 700px) 
  {
    display: block;
  }
`;

const H1 = styled.h1`
  width: 90%;
  margin: 1rem auto;
  font-size: 22px;
  opacity: 0.9;
  text-align: left;
`;


const Comments = ({author, permlink})=>{
  const [loading, setloading] = useState(true);
  const [loadingNew, setloadingNew] = useState(false);
  const [post, setPost] = useState([]);
  const [text,setText] = useState("");

  const {
    auth,
    actions,
    steemConnect
   
    } = useContext(SteemContext);


 useEffect(()=>{

    SteemAsync.getPostReplies(author, permlink).then(
    (response)=>{

      setPost(response)
      console.log(response);
      setloading(false)
    }).catch(
    (err)=>console.error(err)) 
  },[])


  const makePost = ()=>{

  const reply_params = {
   author: author,
   post: permlink,
   body:text,
   permlink: uuid4()
  // jsonmetadata = {}
  }

  setloadingNew(true);

    actions.reply(reply_params).then((res)=>{

      setPost([reply_params,...post]);
      setText("");
        setloadingNew(false)
      

    }).catch((err)=> {
      alert("Error posting comment, try againg.");
      console.log(err);
     setloadingNew(false)
  } )

   }



   /*

  const extensions = {'extensions': [
               [0, {
                   'beneficiaries': [{
                       'account': 'baronjensen',
                       'weight': 5000
                   }]
               }]]
  }*/
/*
  steemConnect.comment(author, permlink, auth.name, `dili${Date.now()}${auth.name}${permlink}${author}ad3ed3q3`, "", text, JSON.stringify({}), function (err, res) {
  console.log(err, res)
});

*/
  

  //console.log(products);
  return (
      <React.Fragment>

        <Container>

        <div id="comments"/>

<H1>Commnets</H1>
{loadingNew&&<h4 style={{width:"100%", textAlign:"center", opacity:"0.4"}}>Creating post, please wait</h4>}
{(auth && !loadingNew) && <React.Fragment>
          <textarea 
            value={text} onChange={(e)=>{setText(e.target.value)}}
            style={{width:"90%", margin:"1rem 5%"}}
            placeholder="Comments..." rows="10">
        
            </textarea>

          <Button 
            style={{ marginLeft:" 5%"}}
            onClick={()=>{
              if(text);
                makePost()
             }}>Submit commnet</Button></React.Fragment>}

{
          <Row>
            {_.map(post,(post)=>(
            <React.Fragment>
            <Card>
            <Link to={"/profile/"+post.author} style={{textDecoration:"none"}}>
            <Title>{post.author}</Title></Link>
            <Subtitle key={post.id}>
             <Markdown 
               
              dangerouslySetInnerHTML={{ __html: parseSteemMarkdown(post.body) }} />
            </Subtitle>

            </Card>
            </React.Fragment>

            ))  
            }
          </Row>}

        </Container>
     
      </React.Fragment>
    );
}

export default Comments;


/*

(!this.props.orders.value.products[product.id] && !this.props.disbled) &&
  <Button  
    onClick={()=>{
      this.props.orders.addProductToCar(product);
    }}>+</Button>
*/